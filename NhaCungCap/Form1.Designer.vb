﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_nhacungcap
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.tbmancc = New System.Windows.Forms.TextBox()
        Me.tbtenncc = New System.Windows.Forms.TextBox()
        Me.tbdiachi = New System.Windows.Forms.TextBox()
        Me.tbdienthoai = New System.Windows.Forms.TextBox()
        Me.dgvnhacungcap = New System.Windows.Forms.DataGridView()
        Me.btthoat = New System.Windows.Forms.Button()
        Me.bthuy = New System.Windows.Forms.Button()
        Me.btluu = New System.Windows.Forms.Button()
        Me.btxoa = New System.Windows.Forms.Button()
        Me.btsua = New System.Windows.Forms.Button()
        Me.btthem = New System.Windows.Forms.Button()
        CType(Me.dgvnhacungcap, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(313, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(77, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Nhà Cung Cấp"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(38, 57)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(95, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Mã Nhà Cung Cấp"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(38, 109)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(99, 13)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Tên Nhà Cung Cấp"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(373, 57)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(41, 13)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Địa Chỉ"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(373, 109)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(59, 13)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Điện Thoại"
        '
        'tbmancc
        '
        Me.tbmancc.Location = New System.Drawing.Point(144, 54)
        Me.tbmancc.Name = "tbmancc"
        Me.tbmancc.Size = New System.Drawing.Size(155, 20)
        Me.tbmancc.TabIndex = 1
        '
        'tbtenncc
        '
        Me.tbtenncc.Location = New System.Drawing.Point(144, 106)
        Me.tbtenncc.Name = "tbtenncc"
        Me.tbtenncc.Size = New System.Drawing.Size(155, 20)
        Me.tbtenncc.TabIndex = 1
        '
        'tbdiachi
        '
        Me.tbdiachi.Location = New System.Drawing.Point(432, 54)
        Me.tbdiachi.Name = "tbdiachi"
        Me.tbdiachi.Size = New System.Drawing.Size(155, 20)
        Me.tbdiachi.TabIndex = 1
        '
        'tbdienthoai
        '
        Me.tbdienthoai.Location = New System.Drawing.Point(432, 106)
        Me.tbdienthoai.Name = "tbdienthoai"
        Me.tbdienthoai.Size = New System.Drawing.Size(155, 20)
        Me.tbdienthoai.TabIndex = 1
        '
        'dgvnhacungcap
        '
        Me.dgvnhacungcap.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvnhacungcap.Location = New System.Drawing.Point(41, 156)
        Me.dgvnhacungcap.Name = "dgvnhacungcap"
        Me.dgvnhacungcap.Size = New System.Drawing.Size(546, 150)
        Me.dgvnhacungcap.TabIndex = 2
        '
        'btthoat
        '
        Me.btthoat.Location = New System.Drawing.Point(512, 312)
        Me.btthoat.Name = "btthoat"
        Me.btthoat.Size = New System.Drawing.Size(75, 23)
        Me.btthoat.TabIndex = 3
        Me.btthoat.Text = "Thoát"
        Me.btthoat.UseVisualStyleBackColor = True
        '
        'bthuy
        '
        Me.bthuy.Location = New System.Drawing.Point(431, 312)
        Me.bthuy.Name = "bthuy"
        Me.bthuy.Size = New System.Drawing.Size(75, 23)
        Me.bthuy.TabIndex = 4
        Me.bthuy.Text = "Hủy"
        Me.bthuy.UseVisualStyleBackColor = True
        '
        'btluu
        '
        Me.btluu.Location = New System.Drawing.Point(350, 312)
        Me.btluu.Name = "btluu"
        Me.btluu.Size = New System.Drawing.Size(75, 23)
        Me.btluu.TabIndex = 5
        Me.btluu.Text = "Lưu"
        Me.btluu.UseVisualStyleBackColor = True
        '
        'btxoa
        '
        Me.btxoa.Location = New System.Drawing.Point(269, 312)
        Me.btxoa.Name = "btxoa"
        Me.btxoa.Size = New System.Drawing.Size(75, 23)
        Me.btxoa.TabIndex = 6
        Me.btxoa.Text = "Xóa"
        Me.btxoa.UseVisualStyleBackColor = True
        '
        'btsua
        '
        Me.btsua.Location = New System.Drawing.Point(188, 312)
        Me.btsua.Name = "btsua"
        Me.btsua.Size = New System.Drawing.Size(75, 23)
        Me.btsua.TabIndex = 7
        Me.btsua.Text = "Sửa"
        Me.btsua.UseVisualStyleBackColor = True
        '
        'btthem
        '
        Me.btthem.Location = New System.Drawing.Point(107, 312)
        Me.btthem.Name = "btthem"
        Me.btthem.Size = New System.Drawing.Size(75, 23)
        Me.btthem.TabIndex = 8
        Me.btthem.Text = "Thêm"
        Me.btthem.UseVisualStyleBackColor = True
        '
        'form_nhacungcap
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(646, 375)
        Me.Controls.Add(Me.btthem)
        Me.Controls.Add(Me.btsua)
        Me.Controls.Add(Me.btxoa)
        Me.Controls.Add(Me.btluu)
        Me.Controls.Add(Me.bthuy)
        Me.Controls.Add(Me.btthoat)
        Me.Controls.Add(Me.dgvnhacungcap)
        Me.Controls.Add(Me.tbdienthoai)
        Me.Controls.Add(Me.tbtenncc)
        Me.Controls.Add(Me.tbdiachi)
        Me.Controls.Add(Me.tbmancc)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "form_nhacungcap"
        Me.Text = "Form1"
        CType(Me.dgvnhacungcap, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents tbmancc As System.Windows.Forms.TextBox
    Friend WithEvents tbtenncc As System.Windows.Forms.TextBox
    Friend WithEvents tbdiachi As System.Windows.Forms.TextBox
    Friend WithEvents tbdienthoai As System.Windows.Forms.TextBox
    Friend WithEvents dgvnhacungcap As System.Windows.Forms.DataGridView
    Friend WithEvents btthoat As System.Windows.Forms.Button
    Friend WithEvents bthuy As System.Windows.Forms.Button
    Friend WithEvents btluu As System.Windows.Forms.Button
    Friend WithEvents btxoa As System.Windows.Forms.Button
    Friend WithEvents btsua As System.Windows.Forms.Button
    Friend WithEvents btthem As System.Windows.Forms.Button

End Class
