﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ETT;
using BUS;
namespace FormNhaCungCap
{
    public partial class Form_nhacungcap : Form
    {
        public Form_nhacungcap()
        {
            InitializeComponent();
        }

        private void Form_nhacungcap_Load(object sender, EventArgs e)
        {
            loadnhacungcap();
        }
        private void loadnhacungcap()
        {
            DataTable dt = BUSNhaCungCap.laydulieunhacungcap();
            dgvnhacungcap.DataSource = dt;
            this.tbmancc.ResetText();
            this.tbtenncc.ResetText();
            this.tbdiachi.ResetText();
            this.tbdienthoai.ResetText();
            this.tbmancc.Enabled = false;
            //this.bthuy.Enabled = false;
            //this.groupBox1.Enabled = false;
            //this.btthem.Enabled = true;
            //this.btsua.Enabled = true;
            //this.btxoa.Enabled = true;
            //this.btthoat.Enabled = true;
        }


        //mã tự tăng
        private void Matutang()
        {

            DataTable dt = BUSNhaCungCap.laydulieunhacungcap();
            dgvnhacungcap.DataSource = dt;
            string s = "";
            if (dt.Rows.Count <= 0)
                s = "MAN001";
            else
            {
                int k;
                s = "MAN";
                k = Convert.ToInt32(dt.Rows[dt.Rows.Count - 1][0].ToString().Substring(3, 3));
                k = k + 1;
                if (k < 10) s = s + "00";
                else if (k < 100)
                    s = s + "0";
                //else if (k < 1000)
                //    s = s + "00";
                //else if (k < 10000)
                //    s = s + "0";
                s = s + k.ToString();
            }
            tbmancc.Text = s;
        }

        private void btthem_Click(object sender, EventArgs e)
        {
            //loadnhacungcap();
            if (tbtenncc.Text == "" || tbdiachi.Text == "")
            {
                MessageBox.Show("Bạn Hãy Nhập Đầy Đủ Thông Tin!", "Thông Báo!");
                return;
            }
            Matutang();
            //Khởi tạo đối tượng họcsinhDTO và gán giá trị thuộc tính tưng đối tượng
            NhaCungCapETT ncce = new NhaCungCapETT();
            ncce.MaNCC1 = tbmancc.Text;
            ncce.TenNCC1 = tbtenncc.Text;
            ncce.DiaChiNCC1 = tbdiachi.Text;
            ncce.DienThoaiNCC1 = tbdienthoai.Text;
            //Xử lý thêm học sinh
            
            if ( BUSNhaCungCap.themnhacungcap(ncce) == true)
            {
                loadnhacungcap();
                MessageBox.Show("Thêm thành công!", "Thông Báo");
                return;
            }

            MessageBox.Show("Thêm thất bại!", "Thông Báo");
            
          
          
        }

        private void btsua_Click(object sender, EventArgs e)
        {
            this.tbmancc.Enabled = false;
           
            if (tbtenncc.Text == "" || tbdiachi.Text == "")
            {
                MessageBox.Show("Bạn cần nhập đầy đủ thông tin", "Thông Báo");
                return;
            }
            NhaCungCapETT ncce = new NhaCungCapETT();
            ncce.MaNCC1 = tbmancc.Text;
            ncce.TenNCC1 = tbtenncc.Text;
            ncce.DiaChiNCC1 = tbdiachi.Text;
            ncce.DienThoaiNCC1 = tbdienthoai.Text;
            if (BUSNhaCungCap.suanhacungcap(ncce) == true)
            {
                loadnhacungcap();
                MessageBox.Show("Cập nhật dữ liệu thành công!", "Thông báo");
                return;
            }
            MessageBox.Show("Cập nhật dữ liệu thất bại!", "Thông báo");
        }

        private void dgvnhacungcap_Click(object sender, EventArgs e)
        {

            int r = dgvnhacungcap.CurrentCell.RowIndex;
            this.tbmancc.Text = dgvnhacungcap.Rows[r].Cells[0].Value.ToString();
            this.tbtenncc.Text = dgvnhacungcap.Rows[r].Cells[1].Value.ToString();
            this.tbdiachi.Text = dgvnhacungcap.Rows[r].Cells[2].Value.ToString();
            this.tbdienthoai.Text = dgvnhacungcap.Rows[r].Cells[3].Value.ToString();
        }

        private void btxoa_Click(object sender, EventArgs e)
        {

            this.tbmancc.Enabled = false;
            NhaCungCapETT ncce = new NhaCungCapETT();
            ncce.MaNCC1 = tbmancc.Text;
            if (BUSNhaCungCap.xoanhacc(ncce) == true)
            {
                loadnhacungcap();
                MessageBox.Show("Xóa thành công!", "Thông báo");
                return;
            }
            MessageBox.Show("Xóa thất bại!", "Thông báo");
        }

        private void btthoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bttimkiem_Click(object sender, EventArgs e)
        {
            this.tbmancc.Enabled = false;
            if (tbtenncc.Text == "")
            {
                MessageBox.Show("Ban Chưa nhập từ khóa","Thông Báo");
                return;
            }
            string timkiem = tbtenncc.Text;
            DataTable dt = BUSNhaCungCap.timkiemncc(timkiem);
            if (dt.Rows.Count == 0)
            {
                MessageBox.Show("Không Tìm Thấy Kết Quả", "Thông Báo");
                return;
            }
            MessageBox.Show(string.Format("Đã Tìm Thấy {0} Kết Quả", dt.Rows.Count), "Thông Báo");
            dgvnhacungcap.DataSource = dt;
        }

        private void bthuy_Click(object sender, EventArgs e)
        {
            this.tbmancc.ResetText();
            this.tbtenncc.ResetText();
            this.tbdiachi.ResetText();
            this.tbdienthoai.ResetText();
            this.tbmancc.Enabled = false;
        }
    }
}
