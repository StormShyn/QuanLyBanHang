﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using BUS;
using ETT;
using FormPhieuXuat;
namespace FormDangNhap
{
    public partial class dn : Form
    {
        //DataTable dt = BUSDangNhap.xemdl(sql);
        public dn()
        {
            InitializeComponent();
        }

        //Cái này là ủy quyền
        public delegate void RunMAIN(int Quyen);
        public RunMAIN Run_MAIN;

        public static string tk;
        public static string mk;
        public static string maquyen;
        DangNhapETT dne = new DangNhapETT();
        BUSDangNhap bdn = new BUSDangNhap();

        private void tbdangnhap_Click(object sender, EventArgs e)
        {
            //DangNhapETT dne = new DangNhapETT();
            dne.TenDN1 = tbdangnhap.Text;
            dne.MatKhau1 = tbmatkhau.Text;


            if (tbdangnhap.Text == "" || tbmatkhau.Text == "")
            {
                MessageBox.Show("Không được bỏ trống các trường!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                if (tbdangnhap.Text == "")
                    tbdangnhap.Focus();
                else tbmatkhau.Focus();

            }
            else
            {
                if (BUSDangNhap.loaddangnhap(dne) == true)
                {

                    tbdangnhap.Text = "";
                    tbmatkhau.Text = "";

                    tk = dne.TenDN1;
                    mk = dne.MatKhau1;
                    string quyen;
                    //DataTable dt = BUSDangNhap.maquyen1(quyen);
                    if(BUSDangNhap.maquyen1(quyen)=="quyen");

                    Run_MAIN(int.Parse(maquyen));

                    MessageBox.Show("Đăng nhập thành công!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Tài khoản hoặc mật khẩu không đúng!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    tbtaikhoan.Text = "";
                    tbmatkhau.Text = "";
                    tbtaikhoan.Focus();
                }
            }
        }
    }

}