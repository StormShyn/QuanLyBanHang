﻿namespace BTL_QLBH
{
    partial class ThemNV
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tbtk = new System.Windows.Forms.TextBox();
            this.tbmk = new System.Windows.Forms.TextBox();
            this.tbht = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.btthem = new System.Windows.Forms.Button();
            this.dgvthemtk = new System.Windows.Forms.DataGridView();
            this.btsua = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.bttimkiem = new System.Windows.Forms.Button();
            this.cbtt = new System.Windows.Forms.ComboBox();
            this.cbquyen = new System.Windows.Forms.ComboBox();
            this.btxoa = new System.Windows.Forms.Button();
            this.btreload = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvthemtk)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Location = new System.Drawing.Point(211, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(184, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Thêm Tài Khoản";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(107, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Tài Khoản";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(107, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Mật Khẩu";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(357, 44);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Quyền";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(107, 98);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Họ Tên";
            // 
            // tbtk
            // 
            this.tbtk.Location = new System.Drawing.Point(176, 44);
            this.tbtk.Name = "tbtk";
            this.tbtk.Size = new System.Drawing.Size(130, 20);
            this.tbtk.TabIndex = 2;
            // 
            // tbmk
            // 
            this.tbmk.Location = new System.Drawing.Point(176, 69);
            this.tbmk.Name = "tbmk";
            this.tbmk.Size = new System.Drawing.Size(130, 20);
            this.tbmk.TabIndex = 2;
            // 
            // tbht
            // 
            this.tbht.Location = new System.Drawing.Point(176, 95);
            this.tbht.Name = "tbht";
            this.tbht.Size = new System.Drawing.Size(130, 20);
            this.tbht.TabIndex = 2;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(481, 336);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "Thoát";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btthem
            // 
            this.btthem.Location = new System.Drawing.Point(33, 336);
            this.btthem.Name = "btthem";
            this.btthem.Size = new System.Drawing.Size(75, 23);
            this.btthem.TabIndex = 5;
            this.btthem.Text = "Thêm";
            this.btthem.UseVisualStyleBackColor = true;
            this.btthem.Click += new System.EventHandler(this.btthem_Click);
            // 
            // dgvthemtk
            // 
            this.dgvthemtk.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvthemtk.Location = new System.Drawing.Point(33, 180);
            this.dgvthemtk.Name = "dgvthemtk";
            this.dgvthemtk.Size = new System.Drawing.Size(523, 150);
            this.dgvthemtk.TabIndex = 6;
            this.dgvthemtk.Click += new System.EventHandler(this.dgvthemtk_Click);
            // 
            // btsua
            // 
            this.btsua.Location = new System.Drawing.Point(154, 336);
            this.btsua.Name = "btsua";
            this.btsua.Size = new System.Drawing.Size(75, 23);
            this.btsua.TabIndex = 7;
            this.btsua.Text = "Sửa";
            this.btsua.UseVisualStyleBackColor = true;
            this.btsua.Click += new System.EventHandler(this.button1_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(357, 70);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Trạng thái";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(115, 151);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(130, 20);
            this.textBox2.TabIndex = 10;
            // 
            // bttimkiem
            // 
            this.bttimkiem.Location = new System.Drawing.Point(33, 151);
            this.bttimkiem.Name = "bttimkiem";
            this.bttimkiem.Size = new System.Drawing.Size(76, 23);
            this.bttimkiem.TabIndex = 11;
            this.bttimkiem.Text = "Tim kiếm";
            this.bttimkiem.UseVisualStyleBackColor = true;
            this.bttimkiem.Click += new System.EventHandler(this.bttimkiem_Click);
            // 
            // cbtt
            // 
            this.cbtt.FormattingEnabled = true;
            this.cbtt.Items.AddRange(new object[] {
            "Không Khóa",
            "Khóa"});
            this.cbtt.Location = new System.Drawing.Point(420, 67);
            this.cbtt.Name = "cbtt";
            this.cbtt.Size = new System.Drawing.Size(136, 21);
            this.cbtt.TabIndex = 12;
            // 
            // cbquyen
            // 
            this.cbquyen.FormattingEnabled = true;
            this.cbquyen.Items.AddRange(new object[] {
            "Admin",
            "Nhân Viên Nhập",
            "Nhân Viên Xuất"});
            this.cbquyen.Location = new System.Drawing.Point(420, 41);
            this.cbquyen.Name = "cbquyen";
            this.cbquyen.Size = new System.Drawing.Size(136, 21);
            this.cbquyen.TabIndex = 13;
            // 
            // btxoa
            // 
            this.btxoa.Location = new System.Drawing.Point(262, 336);
            this.btxoa.Name = "btxoa";
            this.btxoa.Size = new System.Drawing.Size(75, 23);
            this.btxoa.TabIndex = 15;
            this.btxoa.Text = "Xóa";
            this.btxoa.UseVisualStyleBackColor = true;
            this.btxoa.Click += new System.EventHandler(this.btxoa_Click);
            // 
            // btreload
            // 
            this.btreload.Location = new System.Drawing.Point(371, 336);
            this.btreload.Name = "btreload";
            this.btreload.Size = new System.Drawing.Size(75, 23);
            this.btreload.TabIndex = 16;
            this.btreload.Text = "Reload";
            this.btreload.UseVisualStyleBackColor = true;
            this.btreload.Click += new System.EventHandler(this.btreload_Click);
            // 
            // ThemNV
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(594, 384);
            this.Controls.Add(this.btreload);
            this.Controls.Add(this.btxoa);
            this.Controls.Add(this.cbquyen);
            this.Controls.Add(this.cbtt);
            this.Controls.Add(this.bttimkiem);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btsua);
            this.Controls.Add(this.dgvthemtk);
            this.Controls.Add(this.btthem);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.tbht);
            this.Controls.Add(this.tbmk);
            this.Controls.Add(this.tbtk);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "ThemNV";
            this.Text = "TaiKhoan";
            this.Load += new System.EventHandler(this.ThemNV_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvthemtk)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbtk;
        private System.Windows.Forms.TextBox tbmk;
        private System.Windows.Forms.TextBox tbht;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btthem;
        private System.Windows.Forms.DataGridView dgvthemtk;
        private System.Windows.Forms.Button btsua;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button bttimkiem;
        private System.Windows.Forms.ComboBox cbtt;
        private System.Windows.Forms.ComboBox cbquyen;
        private System.Windows.Forms.Button btxoa;
        private System.Windows.Forms.Button btreload;
    }
}