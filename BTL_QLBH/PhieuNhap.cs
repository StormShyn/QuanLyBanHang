﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using BUS;
using ETT;
namespace BTL_QLBH
{
    public partial class PhieuNhapHang : Form
    {
        public PhieuNhapHang()
        {
            InitializeComponent();
        }

        private void PhieuNhapHang_Load(object sender, EventArgs e)
        {
            tbsopn.Enabled = false;
            tbtong.Enabled = false;
            loadphieunhaphang();
            loadnhaccungcap();
            loadhanghoa();
            tinhtongsp();
            //Matutang();
        }
        private void loadphieunhaphang()
        {
            DataTable dt = BUSPhieuNhap.laydulieuphieunhap();
            dgvphieunhap.DataSource = dt;
        }
        private void loadnhaccungcap()
        {
            DataTable dt = BUSNhaCungCap.laydulieunhacungcap();
            cbtenncc.DataSource = dt;
            cbtenncc.DisplayMember = "TenNCC";
            cbtenncc.ValueMember = "MaNCC";

        }
        private void loadhanghoa()
        {
            DataTable dt = BUSHangHoa.laydulieuhanghoa();
            cbtenhang.DataSource = dt;
            cbtenhang.DisplayMember = "TenHang";
            cbtenhang.ValueMember = "MaHang";
        }

        private void dgvphieunhap_Click(object sender, EventArgs e)
        {
            int r = dgvphieunhap.CurrentCell.RowIndex;
            this.tbsopn.Text = dgvphieunhap.Rows[r].Cells[0].Value.ToString();
            //this.cb1.SelectedValue = dgvphieunhap.Rows[r].Cells[1].Value.ToString();
            this.cbtenncc.SelectedValue = dgvphieunhap.Rows[r].Cells[1].Value.ToString();
            //this.tbdiachi.Text = dgvphieunhap.Rows[r].Cells[3].Value.ToString();
           // this.tbsdt.Text = dgvphieunhap.Rows[r].Cells[4].Value.ToString();
            this.cbtenhang.SelectedValue = dgvphieunhap.Rows[r].Cells[2].Value.ToString();

            this.tbsoluongnhap.Text = dgvphieunhap.Rows[r].Cells[4].Value.ToString();
            this.tbgianhap.Text = dgvphieunhap.Rows[r].Cells[5].Value.ToString();
            this.dtngaynhap.Text = dgvphieunhap.Rows[r].Cells[3].Value.ToString();
        }

        private void btthem_Click(object sender, EventArgs e)
        {
            //Matutang();
            if (tbsoluongnhap.Text == "" || tbgianhap.Text == "")
            {
                MessageBox.Show("Bạn Hãy Nhập Đầy Đủ Thông Tin!", "Thông Báo!");
                return;
            }
            Matutang();
            //Khởi tạo đối tượng họcsinhDTO và gán giá trị thuộc tính tưng đối tượng
            PhieuNhapETT pne = new PhieuNhapETT();
            pne.MaNCC1 = cbtenncc.SelectedValue.ToString() ;
            pne.MaHang1 = cbtenhang.SelectedValue.ToString();
            pne.NgayNhap1 = DateTime.Parse(dtngaynhap.Text);
            pne.MaPN1 = tbsopn.Text;
            pne.SoLuongNhap1 = int.Parse(tbsoluongnhap.Text);
            pne.GiaNhap1 = int.Parse(tbgianhap.Text);
            //hs.Malop1 = int.Parse(cblop.SelectedValue.ToString());
            //Xử lý thêm học sinh
            if (BUSPhieuNhap.themphieunhap(pne) == true)
            {
                loadphieunhaphang();
                tinhtongsp();
                MessageBox.Show("Thêm thành công!", "Thông Báo");
                return;
            }

            MessageBox.Show("Thêm thất bại!", "Thông Báo");
               
          
          
        }
        private void Matutang()
        {

            DataTable dt = BUSPhieuNhap.laydulieuphieunhap();
            dgvphieunhap.DataSource = dt;
            string s = "";
            if (dt.Rows.Count <= 0)
                s = "MAPN001";
            else
            {
                int k;
                s = "MAPN";
                k = Convert.ToInt32(dt.Rows[dt.Rows.Count - 1][0].ToString().Substring(4,3));
                k = k + 1;
                if (k < 10) s = s + "00";
                else if (k < 100)
                    s = s + "0";
                //else if (k < 1000)
                //    s = s + "00";
                //else if (k < 10000)
                //    s = s + "0";
                s = s + k.ToString();
            }
            tbsopn.Text = s;


        }

        private void btsua_Click(object sender, EventArgs e)
        {
            //this.tbsopn.Enabled = false;

            if (tbsoluongnhap.Text == "" || tbgianhap.Text == "")
            {
                MessageBox.Show("Bạn cần nhập đầy đủ thông tin", "Thông Báo");
                return;
            }
            PhieuNhapETT pne = new PhieuNhapETT();
            pne.MaNCC1 = cbtenncc.SelectedValue.ToString();
            pne.MaHang1 = cbtenhang.SelectedValue.ToString();
            pne.NgayNhap1 = DateTime.Parse(dtngaynhap.Text);
            pne.MaPN1 = tbsopn.Text;
            pne.SoLuongNhap1 = int.Parse(tbsoluongnhap.Text);
            pne.GiaNhap1 = int.Parse(tbgianhap.Text);
            if (BUSPhieuNhap.suaphieuhang(pne) == true)
            {
                loadphieunhaphang();
                tinhtongsp();
                MessageBox.Show("Cập nhật dữ liệu thành công!", "Thông báo");
                return;
            }
            MessageBox.Show("Cập nhật dữ liệu thất bại!", "Thông báo");
        }

        private void btxoa_Click(object sender, EventArgs e)
        {
            //this.tbtk.Enabled = false;
            //this.tbmapx.Enabled = false;
            if (tbsoluongnhap.Text == "" || tbgianhap.Text == "")
            {
                MessageBox.Show("Bạn Hãy Nhập Đầy Đủ Thông Tin!", "Thông Báo!");
                return;
            }

            //Khởi tạo đối tượng họcsinhDTO và gán giá trị thuộc tính tưng đối tượng
            PhieuNhapETT pne = new PhieuNhapETT();
            pne.MaNCC1 = cbtenncc.SelectedValue.ToString();
            pne.MaHang1 = cbtenhang.SelectedValue.ToString();
            pne.NgayNhap1 = DateTime.Parse(dtngaynhap.Text);
            pne.MaPN1 = tbsopn.Text;
            pne.SoLuongNhap1 = int.Parse(tbsoluongnhap.Text);
            pne.GiaNhap1 = int.Parse(tbgianhap.Text);

            //Xử lý thêm học sinh
            if ( BUSPhieuNhap.xoaphieuhang(pne) == true)
            {
                loadphieunhaphang();
                tinhtongsp();
               // tinhtongsp();
                MessageBox.Show("Xóa thành công!", "Thông Báo");
                return;
            }

            MessageBox.Show("Xóa thất bại!", "Thông Báo");
        }

        private void btreload_Click(object sender, EventArgs e)
        {
            loadphieunhaphang();
            this.tbtk.ResetText();
            this.tbsopn.ResetText();
            this.cbtenncc.ResetText();
            this.cbtenhang.ResetText();
            this.dtngaynhap.ResetText();
            this.tbsoluongnhap.ResetText();
            this.tbgianhap.ResetText();
            //this.tbmapx.Enabled = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //this.tbmapx.Enabled = false;
            if (tbtk.Text == "")
            {
                MessageBox.Show("Ban Chưa nhập từ khóa", "Thông Báo");
                return;
            }
            string timkiem = tbtk.Text;
            DataTable dt = BUSPhieuNhap.timkiempn(timkiem);
            if (dt.Rows.Count == 0)
            {
                MessageBox.Show("Không Tìm Thấy Kết Quả", "Thông Báo");
                return;
            }
            MessageBox.Show(string.Format("Đã Tìm Thấy {0} Kết Quả", dt.Rows.Count), "Thông Báo");
            dgvphieunhap.DataSource = dt;
        }
        public void tinhtongsp()
        {   //Tinh tổng có bn cột
            //int sp = dgvphieuxuat.Rows.Count;
            //sp = sp - 1;
            //textBox1.Text = sp.ToString();

            //Tính tổng all tiền
            int tien =  dgvphieunhap.Rows.Count;
            int thanhtien = 0;
            for (int i = 0; i < tien - 1; i++)
            {
                thanhtien += int.Parse(dgvphieunhap.Rows[i].Cells["ThanhTien"].Value.ToString());
            }
            tbtong.Text = thanhtien.ToString();
        }
        private void tbtk_TextChanged(object sender, EventArgs e)
        {

        }

        private void btthoat_Click(object sender, EventArgs e)
        {
            Mainc pt = new Mainc();
            this.Close();
            pt.Show();
        }

        private void btbaocao_Click(object sender, EventArgs e)
        {
            BaoCaoNhap bcn = new BaoCaoNhap();
            bcn.Show();
        }
    }
    
}
