﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ETT;
using BUS;

namespace BTL_QLBH
{
    public partial class FormKhachHang : Form
    {
        public FormKhachHang()
        {
            InitializeComponent();
        }

        private void FormKhachHang_Load(object sender, EventArgs e)
        {
            this.tbmakh.Enabled = false;
            loadkhachhang();
        }
        private void loadkhachhang()
        {
            DataTable dt = BUSKhachHang.loadkhachhang();
            dgvkhachhang.DataSource = dt;
        }
        //Mã Tự tăng
        private void Matutang()
        {

            DataTable dt = BUSKhachHang.loadkhachhang();
            dgvkhachhang.DataSource = dt;
            string s = "";
            if (dt.Rows.Count <= 0)
                s = "MKH001";
            else
            {
                int k;
                s = "MKH";
                k = Convert.ToInt32(dt.Rows[dt.Rows.Count - 1][0].ToString().Substring(3, 3));
                k = k + 1;
                if (k < 10) s = s + "00";
                else if (k < 100)
                    s = s + "0";
                //else if (k < 1000)
                //    s = s + "00";
                //else if (k < 10000)
                //    s = s + "0";
                s = s + k.ToString();
            }
            tbmakh.Text = s;


        }
        private void dgvkhachhang_Click(object sender, EventArgs e)
        {
            int r = dgvkhachhang.CurrentCell.RowIndex;
            this.tbmakh.Text = dgvkhachhang.Rows[r].Cells[0].Value.ToString();
            this.tbtenkh.Text = dgvkhachhang.Rows[r].Cells[1].Value.ToString();
            this.tbdiachi.Text = dgvkhachhang.Rows[r].Cells[2].Value.ToString();
            this.tbdienthoai.Text = dgvkhachhang.Rows[r].Cells[3].Value.ToString();
        }

        private void btthem_Click(object sender, EventArgs e)
        {
            if (tbtenkh.Text == "" || tbdiachi.Text == "")
            {
                MessageBox.Show("Bạn Cần Nhập Đầy Đủ Thông Tin!", "Thông Báo");
                return;
            }
            Matutang();
            KhachHangETT khe = new KhachHangETT();
            khe.MaKH1 = tbmakh.Text;
            khe.TenKH1 = tbtenkh.Text;
            khe.DiaChiKH1 = tbdiachi.Text;
            khe.DienThoaiKH1 = tbdienthoai.Text;
            if (BUSKhachHang.themkhachhang(khe) == true)
            {
                loadkhachhang();
                MessageBox.Show("Thêm Thành Công", "Thông Báo");
                return;
            }
            MessageBox.Show("Thêm Thất Bại!", "Thông Báo");
        }

        private void btsua_Click(object sender, EventArgs e)
        {
            if (tbtenkh.Text == "" || tbdiachi.Text == "")
            {
                MessageBox.Show("Bạn Cần Nhập Đầy Đủ Thông Tin!", "Thông Báo");
                return;
            }
            //Matutang();
            KhachHangETT khe = new KhachHangETT();
            khe.MaKH1 = tbmakh.Text;
            khe.TenKH1 = tbtenkh.Text;
            khe.DiaChiKH1 = tbdiachi.Text;
            khe.DienThoaiKH1 = tbdienthoai.Text;
            if (BUSKhachHang.suakhachhang(khe) == true)
            {
                loadkhachhang();
                MessageBox.Show("Sửa Thành Công", "Thông Báo");
                return;
            }
            MessageBox.Show("Sửa Thất Bại!", "Thông Báo");
        }

        private void btxoa_Click(object sender, EventArgs e)
        {
            if (tbtenkh.Text == "" || tbdiachi.Text == "")
            {
                MessageBox.Show("Bạn Cần Nhập Đầy Đủ Thông Tin!", "Thông Báo");
                return;
            }
            //Matutang();
            KhachHangETT khe = new KhachHangETT();
            khe.MaKH1 = tbmakh.Text;
             if (BUSKhachHang.xoakhachhang(khe) == true)
            {
                loadkhachhang();
                MessageBox.Show("Xóa Thành Công", "Thông Báo");
                return;
            }
            MessageBox.Show("Xóa Thất Bại!", "Thông Báo");
        }

        private void bthuy_Click(object sender, EventArgs e)
        {
            this.tbmakh.ResetText();
            this.tbtenkh.ResetText();
            this.tbdiachi.ResetText();
            this.tbdienthoai.ResetText();
            this.tbtimkiem.ResetText();
            this.tbmakh.Enabled = false;
            loadkhachhang();
        }

        private void btthoat_Click(object sender, EventArgs e)
        {
            Mainc pt = new Mainc();
            this.Close();
            pt.Show();
        }

        private void bttimkiem_Click(object sender, EventArgs e)
        {
            
            this.tbmakh.Enabled = false;
            if (rbtenkh.Checked == true)
            {
                rbmakh.Checked = false;
                if (tbtimkiem.Text == "")
                {
                    MessageBox.Show("Ban Chưa nhập từ khóa", "Thông Báo");
                    return;
                }
                string timkiem = tbtimkiem.Text;
                DataTable dt = BUSKhachHang.timkiemkhachhang(timkiem);
                if (dt.Rows.Count == 0)
                {
                    MessageBox.Show("Không Tìm Thấy Kết Quả", "Thông Báo");
                    return;
                }
                MessageBox.Show(string.Format("Đã Tìm Thấy {0} Kết Quả", dt.Rows.Count), "Thông Báo");
                dgvkhachhang.DataSource = dt;
            }
            else
            {
                if (tbtimkiem.Text == "")
                {
                    MessageBox.Show("Ban Chưa nhập từ khóa", "Thông Báo");
                    return;
                }
                string timkiem = tbtimkiem.Text;
                DataTable dt = BUSKhachHang.timkiemmakh(timkiem);
                if (dt.Rows.Count == 0)
                {
                    MessageBox.Show("Không Tìm Thấy Kết Quả", "Thông Báo");
                    return;
                }
                MessageBox.Show(string.Format("Đã Tìm Thấy {0} Kết Quả", dt.Rows.Count), "Thông Báo");
                dgvkhachhang.DataSource = dt;
            }
        }
    }
}
