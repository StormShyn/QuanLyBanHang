﻿namespace BTL_QLBH
{
    partial class FormHangHoa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbdvt = new System.Windows.Forms.TextBox();
            this.tbloaihang = new System.Windows.Forms.TextBox();
            this.tbtenhang = new System.Windows.Forms.TextBox();
            this.tbmahang = new System.Windows.Forms.TextBox();
            this.bttimkiem = new System.Windows.Forms.Button();
            this.btthem = new System.Windows.Forms.Button();
            this.thsua = new System.Windows.Forms.Button();
            this.btxoa = new System.Windows.Forms.Button();
            this.Reload = new System.Windows.Forms.Button();
            this.btthoat = new System.Windows.Forms.Button();
            this.dgvhanghoa = new System.Windows.Forms.DataGridView();
            this.tbtimkiem = new System.Windows.Forms.TextBox();
            this.rbmh = new System.Windows.Forms.RadioButton();
            this.rbth = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvhanghoa)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Location = new System.Drawing.Point(223, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Hàng Hóa";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Mã Hàng";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 57);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Tên Hàng";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 83);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Loại Hàng";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 112);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Đơn Vị Tính";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbdvt);
            this.groupBox1.Controls.Add(this.tbloaihang);
            this.groupBox1.Controls.Add(this.tbtenhang);
            this.groupBox1.Controls.Add(this.tbmahang);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(40, 52);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(293, 137);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông Tin Hàng Hóa";
            // 
            // tbdvt
            // 
            this.tbdvt.Location = new System.Drawing.Point(106, 109);
            this.tbdvt.Name = "tbdvt";
            this.tbdvt.Size = new System.Drawing.Size(181, 20);
            this.tbdvt.TabIndex = 4;
            // 
            // tbloaihang
            // 
            this.tbloaihang.Location = new System.Drawing.Point(106, 80);
            this.tbloaihang.Name = "tbloaihang";
            this.tbloaihang.Size = new System.Drawing.Size(181, 20);
            this.tbloaihang.TabIndex = 3;
            // 
            // tbtenhang
            // 
            this.tbtenhang.Location = new System.Drawing.Point(106, 54);
            this.tbtenhang.Name = "tbtenhang";
            this.tbtenhang.Size = new System.Drawing.Size(181, 20);
            this.tbtenhang.TabIndex = 2;
            // 
            // tbmahang
            // 
            this.tbmahang.Location = new System.Drawing.Point(106, 27);
            this.tbmahang.Name = "tbmahang";
            this.tbmahang.Size = new System.Drawing.Size(181, 20);
            this.tbmahang.TabIndex = 1;
            // 
            // bttimkiem
            // 
            this.bttimkiem.Location = new System.Drawing.Point(40, 205);
            this.bttimkiem.Name = "bttimkiem";
            this.bttimkiem.Size = new System.Drawing.Size(75, 23);
            this.bttimkiem.TabIndex = 10;
            this.bttimkiem.Text = "Tìm Kiếm";
            this.bttimkiem.UseVisualStyleBackColor = true;
            this.bttimkiem.Click += new System.EventHandler(this.bttimkiem_Click);
            // 
            // btthem
            // 
            this.btthem.Location = new System.Drawing.Point(339, 72);
            this.btthem.Name = "btthem";
            this.btthem.Size = new System.Drawing.Size(75, 23);
            this.btthem.TabIndex = 5;
            this.btthem.Text = "Thêm";
            this.btthem.UseVisualStyleBackColor = true;
            this.btthem.Click += new System.EventHandler(this.btthem_Click);
            // 
            // thsua
            // 
            this.thsua.Location = new System.Drawing.Point(420, 72);
            this.thsua.Name = "thsua";
            this.thsua.Size = new System.Drawing.Size(75, 23);
            this.thsua.TabIndex = 6;
            this.thsua.Text = "Sửa";
            this.thsua.UseVisualStyleBackColor = true;
            this.thsua.Click += new System.EventHandler(this.thsua_Click);
            // 
            // btxoa
            // 
            this.btxoa.Location = new System.Drawing.Point(422, 104);
            this.btxoa.Name = "btxoa";
            this.btxoa.Size = new System.Drawing.Size(75, 23);
            this.btxoa.TabIndex = 7;
            this.btxoa.Text = "Xóa";
            this.btxoa.UseVisualStyleBackColor = true;
            this.btxoa.Click += new System.EventHandler(this.btxoa_Click);
            // 
            // Reload
            // 
            this.Reload.Location = new System.Drawing.Point(339, 104);
            this.Reload.Name = "Reload";
            this.Reload.Size = new System.Drawing.Size(75, 23);
            this.Reload.TabIndex = 8;
            this.Reload.Text = "Reload";
            this.Reload.UseVisualStyleBackColor = true;
            this.Reload.Click += new System.EventHandler(this.huy_Click);
            // 
            // btthoat
            // 
            this.btthoat.Location = new System.Drawing.Point(422, 135);
            this.btthoat.Name = "btthoat";
            this.btthoat.Size = new System.Drawing.Size(75, 23);
            this.btthoat.TabIndex = 14;
            this.btthoat.Text = "Thoát";
            this.btthoat.UseVisualStyleBackColor = true;
            this.btthoat.Click += new System.EventHandler(this.btthoat_Click);
            // 
            // dgvhanghoa
            // 
            this.dgvhanghoa.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvhanghoa.Location = new System.Drawing.Point(40, 234);
            this.dgvhanghoa.Name = "dgvhanghoa";
            this.dgvhanghoa.Size = new System.Drawing.Size(455, 150);
            this.dgvhanghoa.TabIndex = 13;
            this.dgvhanghoa.Click += new System.EventHandler(this.dgvhanghoa_Click);
            // 
            // tbtimkiem
            // 
            this.tbtimkiem.Location = new System.Drawing.Point(146, 205);
            this.tbtimkiem.Name = "tbtimkiem";
            this.tbtimkiem.Size = new System.Drawing.Size(181, 20);
            this.tbtimkiem.TabIndex = 9;
            // 
            // rbmh
            // 
            this.rbmh.AutoSize = true;
            this.rbmh.Checked = true;
            this.rbmh.Location = new System.Drawing.Point(339, 205);
            this.rbmh.Name = "rbmh";
            this.rbmh.Size = new System.Drawing.Size(69, 17);
            this.rbmh.TabIndex = 11;
            this.rbmh.TabStop = true;
            this.rbmh.Text = "Mã Hàng";
            this.rbmh.UseVisualStyleBackColor = true;
            // 
            // rbth
            // 
            this.rbth.AutoSize = true;
            this.rbth.Location = new System.Drawing.Point(430, 206);
            this.rbth.Name = "rbth";
            this.rbth.Size = new System.Drawing.Size(73, 17);
            this.rbth.TabIndex = 12;
            this.rbth.TabStop = true;
            this.rbth.Text = "Tên Hàng";
            this.rbth.UseVisualStyleBackColor = true;
            // 
            // FormHangHoa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(541, 397);
            this.Controls.Add(this.rbth);
            this.Controls.Add(this.rbmh);
            this.Controls.Add(this.tbtimkiem);
            this.Controls.Add(this.dgvhanghoa);
            this.Controls.Add(this.btthoat);
            this.Controls.Add(this.Reload);
            this.Controls.Add(this.btxoa);
            this.Controls.Add(this.thsua);
            this.Controls.Add(this.btthem);
            this.Controls.Add(this.bttimkiem);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Name = "FormHangHoa";
            this.Text = "Hàng Hóa";
            this.Load += new System.EventHandler(this.FormHangHoa_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvhanghoa)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbdvt;
        private System.Windows.Forms.TextBox tbloaihang;
        private System.Windows.Forms.TextBox tbtenhang;
        private System.Windows.Forms.TextBox tbmahang;
        private System.Windows.Forms.Button bttimkiem;
        private System.Windows.Forms.Button btthem;
        private System.Windows.Forms.Button thsua;
        private System.Windows.Forms.Button btxoa;
        private System.Windows.Forms.Button Reload;
        private System.Windows.Forms.Button btthoat;
        private System.Windows.Forms.DataGridView dgvhanghoa;
        private System.Windows.Forms.TextBox tbtimkiem;
        private System.Windows.Forms.RadioButton rbmh;
        private System.Windows.Forms.RadioButton rbth;
    }
}

