﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ETT;
using BUS;
namespace BTL_QLBH
{
    public partial class Mainc : Form
    {
        public Mainc()
        {
            InitializeComponent();
            //label11.Text = quyen;
        }

        private void Main_Load(object sender, EventArgs e)
        {

            //this.lbquyen.Text = DangNhap.ten;
            if (DangNhap.quyen == "Admin")
            {
                this.lbquyen.Text ="Chức vụ: "+ DangNhap.quyen;
                this.lbht.Text = "Họ Tên: " + DangNhap.ten;
                //button3.Enabled = false;
            }
            else if (DangNhap.quyen == "Nhân Viên Nhập")
            {
                this.lbquyen.Text = "Chức vụ: " + DangNhap.quyen;
                this.lbht.Text = "Họ Tên: " + DangNhap.ten;
                btkh.Enabled = false;
                btpx.Enabled = false;
                button1.Enabled = false;
            }
            else
            {
                this.lbquyen.Text = "Chức vụ: " + DangNhap.quyen;
                this.lbht.Text = "Họ Tên: " + DangNhap.ten;
                btncc.Enabled = false;
                btpn.Enabled = false;
                button1.Enabled = false;
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form_nhacungcap ncc = new Form_nhacungcap();
            this.Close();
            ncc.Show();
        }

        private void btpn_Click(object sender, EventArgs e)
        {
            PhieuNhapHang pn = new PhieuNhapHang();
            this.Close();
            pn.Show();
        }

        private void bthh_Click(object sender, EventArgs e)
        {
            FormHangHoa hh = new FormHangHoa();
            this.Close();
            hh.Show();
        }

        private void btkh_Click(object sender, EventArgs e)
        {
            FormKhachHang kh = new FormKhachHang();
            this.Close();
            kh.Show();
        }

        private void btpx_Click(object sender, EventArgs e)
        {
            PhieuXuat px = new PhieuXuat();
            this.Close();
            px.Show();
        }

        private void btthoat_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            ThemNV tnv = new ThemNV();
            this.Close();
            tnv.Show();
        }
    }
}
