﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ETT;
using BUS;
using System.Data.SqlClient;

//using FormNhaCungCap;
namespace BTL_QLBH
{
    public partial class DangNhap : Form
    {
        public static string quyen, ten;
        SqlConnection conn;
        public DangNhap()
        {
            InitializeComponent();
            conn = new SqlConnection(@"Data Source=QHONLINE-PC;Initial Catalog=BTL_QLBH;Integrated Security=True");
        }
        private void DangNhap_Load(object sender, EventArgs e)
        {

        }
        private void btthoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btdangnhap_Click(object sender, EventArgs e)
        {

            //kiem tra dang nhap
            if (tbtaikhoan.Text == "" && tbmatkhau.Text == "")
            {
                MessageBox.Show(" Chưa nhập thông tin ","Thông Báo");
            }
            else if (tbtaikhoan.Text == "" && tbmatkhau.Text != "")
            {
                MessageBox.Show(" Chưa nhập tên đăng nhập ","Thông Báo");
            }
            else if (tbtaikhoan.Text != "" && tbmatkhau.Text == "")
            {
                MessageBox.Show(" Chưa nhập mật khẩu ","Thông Báo");
            }
            else
            {

                string sqlTenDangNhap = "Select TenDN from DangNhap where TenDN = '" + tbtaikhoan.Text + "'";
                SqlDataAdapter ad = new SqlDataAdapter(sqlTenDangNhap, conn);
                DataTable dt = new DataTable();
                dt.Clear();
                ad.Fill(dt);
                if (dt.Rows.Count != 0)
                {
                    //kiem tra xem tai khoan co khoa khong
                    int loi = Convert.ToInt16(this.exeCuteQuery(tbtaikhoan.Text));
                    if (loi >= 5)
                    {

                        if (loi == 5)
                        {
                            MessageBox.Show("Tài khoản bị khóa","Thông Báo");
                            //update trang thai = khoa
                            string sqlkhoa = "update DangNhap set TrangThai= 'Khóa' where TenDN ='" + tbtaikhoan.Text + "'";
                            //string sqlkhoa = "update taikhoan set trangthai=" +1+ "where tendangnhap ='" + tbtaikhoan.Text + "'";
                            if (conn.State == ConnectionState.Closed)
                            {
                                conn.Open();
                            }
                            SqlCommand cmd = new SqlCommand(sqlkhoa, conn);

                            cmd.ExecuteNonQuery();
                        }
                        else
                        {
                            MessageBox.Show("Tài khoản bị khóa","Thông Báo");
                        }
                    }
                    else
                    {
                        string sqlMatKhau = "Select MatKhau from DangNhap where MatKhau = '" + tbmatkhau.Text + "'";
                        SqlDataAdapter ad1 = new SqlDataAdapter(sqlMatKhau, conn);
                        DataTable dt1 = new DataTable();
                        dt1.Clear();
                        ad1.Fill(dt1);
                        if (dt1.Rows.Count != 0)
                        {
                            this.Hide();
                            MessageBox.Show(" Đăng nhập thành công! ","Thông Báo");
                            string sql = "update DangNhap set loi=" + 0 + " where TenDN ='" + tbtaikhoan.Text + "'";
                            this.loi(sql);

                            //set quyen
                            string sqlquyen = "Select Quyen from DangNhap where TenDN = '" + tbtaikhoan.Text + "'";
                            SqlDataAdapter adquyen = new SqlDataAdapter(sqlquyen, conn);
                            DataTable dtquyen = new DataTable();
                            dtquyen.Clear();
                            adquyen.Fill(dtquyen);
                            quyen = dtquyen.Rows[0][0].ToString().Trim();

                            // lay ten
                            string sqlten = "Select HoTen from DangNhap where TenDN = '" + tbtaikhoan.Text + "'";
                            SqlDataAdapter adten = new SqlDataAdapter(sqlten, conn);
                            DataTable dtten = new DataTable();
                            dtten.Clear();
                            adten.Fill(dtten);
                            ten = dtten.Rows[0][0].ToString().Trim();

                            Mainc main = new Mainc();
                            main.Show();
                        }
                        else
                        {
                            MessageBox.Show(" Sai mật khẩu ","Thông Báo");
                            this.tbmatkhau.Text = "";
                            string sql = "update DangNhap set loi=" + (loi + 1) + " where TenDN ='" + tbtaikhoan.Text + "'";
                            this.loi(sql);
                        }
                    }
                }
                else
                {
                    MessageBox.Show(" Sai tên đăng nhập ","Thông Báo");
                }
            }
        }
        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Mainc main = new Mainc();
            main.Show();
        }
        private string exeCuteQuery(string taikhoan)
        {

            string sqlMatKhau = "Select loi from DangNhap where TenDN = '" + taikhoan + "'";
            SqlDataAdapter ad2 = new SqlDataAdapter(sqlMatKhau, conn);
            DataTable dt2 = new DataTable();
            dt2.Clear();
            ad2.Fill(dt2);
            return dt2.Rows[0][0].ToString();

        }

        private void loi(string sql)
        {
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.ExecuteNonQuery();
        }
    }

}
