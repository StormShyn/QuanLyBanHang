﻿namespace BTL_QLBH
{
    partial class BaoCaoNhap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.ThongKeNhapBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.BTL_QLBHDataSet1 = new BTL_QLBH.BTL_QLBHDataSet1();
            this.DangNhapBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.reportnhapBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.BTL_QLBHDataSet = new BTL_QLBH.BTL_QLBHDataSet();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.reportnhapTableAdapter = new BTL_QLBH.BTL_QLBHDataSetTableAdapters.reportnhapTableAdapter();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dtfromyear = new System.Windows.Forms.DateTimePicker();
            this.dttoyear = new System.Windows.Forms.DateTimePicker();
            this.btthongke = new System.Windows.Forms.Button();
            this.DangNhapTableAdapter = new BTL_QLBH.BTL_QLBHDataSet1TableAdapters.DangNhapTableAdapter();
            this.ThongKeNhapTableAdapter = new BTL_QLBH.BTL_QLBHDataSet1TableAdapters.ThongKeNhapTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.ThongKeNhapBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BTL_QLBHDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DangNhapBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportnhapBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BTL_QLBHDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // ThongKeNhapBindingSource
            // 
            this.ThongKeNhapBindingSource.DataMember = "ThongKeNhap";
            this.ThongKeNhapBindingSource.DataSource = this.BTL_QLBHDataSet1;
            // 
            // BTL_QLBHDataSet1
            // 
            this.BTL_QLBHDataSet1.DataSetName = "BTL_QLBHDataSet1";
            this.BTL_QLBHDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // DangNhapBindingSource
            // 
            this.DangNhapBindingSource.DataMember = "DangNhap";
            this.DangNhapBindingSource.DataSource = this.BTL_QLBHDataSet1;
            // 
            // reportnhapBindingSource
            // 
            this.reportnhapBindingSource.DataMember = "reportnhap";
            this.reportnhapBindingSource.DataSource = this.BTL_QLBHDataSet;
            // 
            // BTL_QLBHDataSet
            // 
            this.BTL_QLBHDataSet.DataSetName = "BTL_QLBHDataSet";
            this.BTL_QLBHDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // reportViewer1
            // 
            reportDataSource1.Name = "DataSet1";
            reportDataSource1.Value = this.ThongKeNhapBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "BTL_QLBH.Report5.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(22, 121);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(625, 246);
            this.reportViewer1.TabIndex = 0;
            // 
            // reportnhapTableAdapter
            // 
            this.reportnhapTableAdapter.ClearBeforeFill = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Location = new System.Drawing.Point(177, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(303, 29);
            this.label1.TabIndex = 1;
            this.label1.Text = "THỐNG KÊ NHẬP HÀNG";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Từ Ngày:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(226, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Đến Ngày:";
            // 
            // dtfromyear
            // 
            this.dtfromyear.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtfromyear.Location = new System.Drawing.Point(76, 89);
            this.dtfromyear.Name = "dtfromyear";
            this.dtfromyear.Size = new System.Drawing.Size(144, 20);
            this.dtfromyear.TabIndex = 2;
            // 
            // dttoyear
            // 
            this.dttoyear.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dttoyear.Location = new System.Drawing.Point(290, 89);
            this.dttoyear.Name = "dttoyear";
            this.dttoyear.Size = new System.Drawing.Size(144, 20);
            this.dttoyear.TabIndex = 2;
            // 
            // btthongke
            // 
            this.btthongke.Location = new System.Drawing.Point(452, 89);
            this.btthongke.Name = "btthongke";
            this.btthongke.Size = new System.Drawing.Size(75, 23);
            this.btthongke.TabIndex = 3;
            this.btthongke.Text = "Thống Kê";
            this.btthongke.UseVisualStyleBackColor = true;
            this.btthongke.Click += new System.EventHandler(this.btthongke_Click);
            // 
            // DangNhapTableAdapter
            // 
            this.DangNhapTableAdapter.ClearBeforeFill = true;
            // 
            // ThongKeNhapTableAdapter
            // 
            this.ThongKeNhapTableAdapter.ClearBeforeFill = true;
            // 
            // BaoCaoNhap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(670, 388);
            this.Controls.Add(this.btthongke);
            this.Controls.Add(this.dttoyear);
            this.Controls.Add(this.dtfromyear);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.reportViewer1);
            this.Name = "BaoCaoNhap";
            this.Text = "BaoCaoNhap";
            this.Load += new System.EventHandler(this.BaoCaoNhap_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ThongKeNhapBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BTL_QLBHDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DangNhapBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportnhapBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BTL_QLBHDataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource reportnhapBindingSource;
        private BTL_QLBHDataSet BTL_QLBHDataSet;
        private BTL_QLBHDataSetTableAdapters.reportnhapTableAdapter reportnhapTableAdapter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtfromyear;
        private System.Windows.Forms.DateTimePicker dttoyear;
        private System.Windows.Forms.Button btthongke;
        private System.Windows.Forms.BindingSource DangNhapBindingSource;
        private BTL_QLBHDataSet1 BTL_QLBHDataSet1;
        private BTL_QLBHDataSet1TableAdapters.DangNhapTableAdapter DangNhapTableAdapter;
        private System.Windows.Forms.BindingSource ThongKeNhapBindingSource;
        private BTL_QLBHDataSet1TableAdapters.ThongKeNhapTableAdapter ThongKeNhapTableAdapter;
    }
}