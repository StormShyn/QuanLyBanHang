﻿namespace BTL_QLBH
{
    partial class PhieuNhapHang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dtngaynhap = new System.Windows.Forms.DateTimePicker();
            this.cbtenhang = new System.Windows.Forms.ComboBox();
            this.cbtenncc = new System.Windows.Forms.ComboBox();
            this.tbgianhap = new System.Windows.Forms.TextBox();
            this.tbsoluongnhap = new System.Windows.Forms.TextBox();
            this.tbsopn = new System.Windows.Forms.TextBox();
            this.dgvphieunhap = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.btthoat = new System.Windows.Forms.Button();
            this.btreload = new System.Windows.Forms.Button();
            this.btxoa = new System.Windows.Forms.Button();
            this.btsua = new System.Windows.Forms.Button();
            this.btthem = new System.Windows.Forms.Button();
            this.tbtk = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tbtong = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btbaocao = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvphieunhap)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(288, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nhập Hàng";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Mã Phiếu Nhập";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(115, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Tên Nhà Cung Cấp";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(20, 85);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Tên Hàng";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(335, 34);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(70, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Ngày Nhập";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(335, 88);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Giá Nhập";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(335, 59);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(95, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Số Lượng Nhập";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dtngaynhap);
            this.groupBox1.Controls.Add(this.cbtenhang);
            this.groupBox1.Controls.Add(this.cbtenncc);
            this.groupBox1.Controls.Add(this.tbgianhap);
            this.groupBox1.Controls.Add(this.tbsoluongnhap);
            this.groupBox1.Controls.Add(this.tbsopn);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(24, 52);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(647, 127);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Nhập Thông Tin";
            // 
            // dtngaynhap
            // 
            this.dtngaynhap.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtngaynhap.Location = new System.Drawing.Point(429, 28);
            this.dtngaynhap.Name = "dtngaynhap";
            this.dtngaynhap.Size = new System.Drawing.Size(171, 20);
            this.dtngaynhap.TabIndex = 12;
            // 
            // cbtenhang
            // 
            this.cbtenhang.FormattingEnabled = true;
            this.cbtenhang.Location = new System.Drawing.Point(133, 85);
            this.cbtenhang.Name = "cbtenhang";
            this.cbtenhang.Size = new System.Drawing.Size(171, 21);
            this.cbtenhang.TabIndex = 11;
            // 
            // cbtenncc
            // 
            this.cbtenncc.FormattingEnabled = true;
            this.cbtenncc.Location = new System.Drawing.Point(133, 56);
            this.cbtenncc.Name = "cbtenncc";
            this.cbtenncc.Size = new System.Drawing.Size(171, 21);
            this.cbtenncc.TabIndex = 10;
            // 
            // tbgianhap
            // 
            this.tbgianhap.Location = new System.Drawing.Point(429, 82);
            this.tbgianhap.Name = "tbgianhap";
            this.tbgianhap.Size = new System.Drawing.Size(171, 20);
            this.tbgianhap.TabIndex = 1;
            // 
            // tbsoluongnhap
            // 
            this.tbsoluongnhap.Location = new System.Drawing.Point(429, 54);
            this.tbsoluongnhap.Name = "tbsoluongnhap";
            this.tbsoluongnhap.Size = new System.Drawing.Size(171, 20);
            this.tbsoluongnhap.TabIndex = 1;
            // 
            // tbsopn
            // 
            this.tbsopn.Location = new System.Drawing.Point(133, 31);
            this.tbsopn.Name = "tbsopn";
            this.tbsopn.Size = new System.Drawing.Size(171, 20);
            this.tbsopn.TabIndex = 1;
            // 
            // dgvphieunhap
            // 
            this.dgvphieunhap.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvphieunhap.Location = new System.Drawing.Point(24, 249);
            this.dgvphieunhap.Name = "dgvphieunhap";
            this.dgvphieunhap.Size = new System.Drawing.Size(647, 225);
            this.dgvphieunhap.TabIndex = 2;
            this.dgvphieunhap.Click += new System.EventHandler(this.dgvphieunhap_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(6, 17);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Tìm Kiếm";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btthoat
            // 
            this.btthoat.Location = new System.Drawing.Point(596, 480);
            this.btthoat.Name = "btthoat";
            this.btthoat.Size = new System.Drawing.Size(75, 23);
            this.btthoat.TabIndex = 4;
            this.btthoat.Text = "Thoat";
            this.btthoat.UseVisualStyleBackColor = true;
            this.btthoat.Click += new System.EventHandler(this.btthoat_Click);
            // 
            // btreload
            // 
            this.btreload.Location = new System.Drawing.Point(24, 480);
            this.btreload.Name = "btreload";
            this.btreload.Size = new System.Drawing.Size(75, 23);
            this.btreload.TabIndex = 5;
            this.btreload.Text = "Reload";
            this.btreload.UseVisualStyleBackColor = true;
            this.btreload.Click += new System.EventHandler(this.btreload_Click);
            // 
            // btxoa
            // 
            this.btxoa.Location = new System.Drawing.Point(515, 480);
            this.btxoa.Name = "btxoa";
            this.btxoa.Size = new System.Drawing.Size(75, 23);
            this.btxoa.TabIndex = 7;
            this.btxoa.Text = "Xóa";
            this.btxoa.UseVisualStyleBackColor = true;
            this.btxoa.Click += new System.EventHandler(this.btxoa_Click);
            // 
            // btsua
            // 
            this.btsua.Location = new System.Drawing.Point(434, 480);
            this.btsua.Name = "btsua";
            this.btsua.Size = new System.Drawing.Size(75, 23);
            this.btsua.TabIndex = 8;
            this.btsua.Text = "Sửa";
            this.btsua.UseVisualStyleBackColor = true;
            this.btsua.Click += new System.EventHandler(this.btsua_Click);
            // 
            // btthem
            // 
            this.btthem.Location = new System.Drawing.Point(353, 480);
            this.btthem.Name = "btthem";
            this.btthem.Size = new System.Drawing.Size(75, 23);
            this.btthem.TabIndex = 9;
            this.btthem.Text = "Thêm";
            this.btthem.UseVisualStyleBackColor = true;
            this.btthem.Click += new System.EventHandler(this.btthem_Click);
            // 
            // tbtk
            // 
            this.tbtk.Location = new System.Drawing.Point(88, 19);
            this.tbtk.Name = "tbtk";
            this.tbtk.Size = new System.Drawing.Size(125, 20);
            this.tbtk.TabIndex = 10;
            this.tbtk.TextChanged += new System.EventHandler(this.tbtk_TextChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.tbtk);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(24, 185);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(304, 45);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Tìm Kiếm Theo Mã Phiếu Nhập";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tbtong);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(471, 189);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(200, 41);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Tổng Tiền";
            // 
            // tbtong
            // 
            this.tbtong.Location = new System.Drawing.Point(7, 14);
            this.tbtong.Name = "tbtong";
            this.tbtong.Size = new System.Drawing.Size(140, 20);
            this.tbtong.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(153, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "VNĐ";
            // 
            // btbaocao
            // 
            this.btbaocao.Location = new System.Drawing.Point(105, 480);
            this.btbaocao.Name = "btbaocao";
            this.btbaocao.Size = new System.Drawing.Size(75, 23);
            this.btbaocao.TabIndex = 13;
            this.btbaocao.Text = "Thống Kê";
            this.btbaocao.UseVisualStyleBackColor = true;
            this.btbaocao.Click += new System.EventHandler(this.btbaocao_Click);
            // 
            // PhieuNhapHang
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(692, 514);
            this.Controls.Add(this.btbaocao);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btthem);
            this.Controls.Add(this.btsua);
            this.Controls.Add(this.btxoa);
            this.Controls.Add(this.btreload);
            this.Controls.Add(this.btthoat);
            this.Controls.Add(this.dgvphieunhap);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Name = "PhieuNhapHang";
            this.Text = "Phiếu Nhập Hàng";
            this.Load += new System.EventHandler(this.PhieuNhapHang_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvphieunhap)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbgianhap;
        private System.Windows.Forms.TextBox tbsoluongnhap;
        private System.Windows.Forms.TextBox tbsopn;
        private System.Windows.Forms.DataGridView dgvphieunhap;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btthoat;
        private System.Windows.Forms.Button btreload;
        private System.Windows.Forms.Button btxoa;
        private System.Windows.Forms.Button btsua;
        private System.Windows.Forms.Button btthem;
        private System.Windows.Forms.ComboBox cbtenncc;
        private System.Windows.Forms.ComboBox cbtenhang;
        private System.Windows.Forms.DateTimePicker dtngaynhap;
        private System.Windows.Forms.TextBox tbtk;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox tbtong;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btbaocao;
    }
}

