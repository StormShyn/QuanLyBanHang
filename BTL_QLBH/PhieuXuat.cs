﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ETT;
using BUS;

namespace BTL_QLBH
{
   
    public partial class PhieuXuat : Form
    {
        public PhieuXuat()
        {
            InitializeComponent();
        }
        public static string quyen;
        private void Form1_Load(object sender, EventArgs e)
        {
            
                loadphieuxuat();
                loadkhachhang();
                loadhanghoa();
                tinhtongsp();
           
                
        }
        private void loadphieuxuat()
        {
            
            this.textBox1.Enabled = false;
            this.tbmapx.Enabled = false;
            DataTable dt = BUSPhieuXuat.laydulieuphieuxuat();
            dgvphieuxuat.DataSource = dt;

        }
               private void loadhanghoa()
        {
            DataTable dt = BUSHangHoa.laydulieuhanghoa();
            cbtenhang.DataSource = dt;
            cbtenhang.DisplayMember = "TenHang";
            cbtenhang.ValueMember = "MaHang";
        }
               private void loadkhachhang()
               {
                   DataTable dt = BUSKhachHang.loadkhachhang();
                   cbtenkh.DataSource = dt;
                   cbtenkh.DisplayMember = "TenKH";
                   cbtenkh.ValueMember = "MaKH";
               }
        private void Matutang()
        {

            DataTable dt = BUSPhieuXuat.laydulieuphieuxuat();
            dgvphieuxuat.DataSource = dt;
            string s = "";
            if (dt.Rows.Count <= 0)
                s = "MAPX0001";
            else
            {
                int k;
                s = "MAPX";
                k = Convert.ToInt32(dt.Rows[dt.Rows.Count - 1][0].ToString().Substring(4, 4));
                k = k + 1;
                if (k < 10) s = s + "000";
                else if (k < 100)
                    s = s + "00";
                else if (k < 1000)
                    s = s + "0";
                //else if (k < 10000)
                //    s = s + "0";
                s = s + k.ToString();
            }
            tbmapx.Text = s;
        }

        private void dgvphieuxuat_Click(object sender, EventArgs e)
        {
            int r = dgvphieuxuat.CurrentCell.RowIndex;
            this.tbmapx.Text = dgvphieuxuat.Rows[r].Cells[0].Value.ToString();
            this.cbtenkh.SelectedValue = dgvphieuxuat.Rows[r].Cells[1].Value.ToString();
            //this.tbtenkh.Text = dgvphieuxuat.Rows[r].Cells[1].Value.ToString();
            //this.tbdiachi.Text = dgvphieuxuat.Rows[r].Cells[2].Value.ToString();
            //this.tbsdt.Text = dgvphieuxuat.Rows[r].Cells[3].Value.ToString();
            this.cbtenhang.SelectedValue = dgvphieuxuat.Rows[r].Cells[2].Value.ToString();
            this.dtngayban.Text = dgvphieuxuat.Rows[r].Cells[4].Value.ToString();
            this.tbsoluong.Text = dgvphieuxuat.Rows[r].Cells[5].Value.ToString();
            this.tbgiaban.Text = dgvphieuxuat.Rows[r].Cells[6].Value.ToString();
            //dgvphieuxuat.CurrentRow.Cells[8].Value = (int.Parse(dgvphieuxuat.CurrentRow.Cells[6].Value.ToString()) * int.Parse(dgvphieuxuat.CurrentRow.Cells[7].Value.ToString()));
            
        }

        public static void hangcon()
        {

        }
        private void btthem_Click(object sender, EventArgs e)
        {
            this.textBox1.Enabled = false;
            this.tbmapx.Enabled = false;
            if (tbsoluong.Text == ""||tbgiaban.Text =="")
            {
                MessageBox.Show("Bạn Hãy Nhập Đầy Đủ Thông Tin!", "Thông Báo!");
                return;
            }
            Matutang();
            PhieuXuatETT pxe = new PhieuXuatETT();
            //pxe.TenKH1 = tbtenkh.Text;
            //pxe.DiaChi1 = tbdiachi.Text;
            //pxe.DienThoai1 = tbsdt.Text;
            pxe.MaKH1 = cbtenkh.SelectedValue.ToString();
            pxe.MaHang1 = cbtenhang.SelectedValue.ToString();
            pxe.NgayBan1 = DateTime.Parse(dtngayban.Text);
            pxe.MaPX1 = tbmapx.Text;
            pxe.SoLuongBan1 = int.Parse(tbsoluong.Text);
            pxe.GiaBan1 = int.Parse(tbgiaban.Text);
            //Xử lý thêm học sinh
            if ( BUSPhieuXuat.themphieuxuat(pxe) == true)
            {
                loadphieuxuat();
                tinhtongsp();
                MessageBox.Show("Thêm thành công!", "Thông Báo");
                return;
            }

            MessageBox.Show("Thêm thất bại!", "Thông Báo");
        }

        private void btsua_Click(object sender, EventArgs e)
        {
            this.textBox1.Enabled = false;
            this.tbmapx.Enabled = false;
            if (tbsoluong.Text == "" || tbgiaban.Text == "")
            {
                MessageBox.Show("Bạn Hãy Nhập Đầy Đủ Thông Tin!", "Thông Báo!");
                return;
            }
            
            //Khởi tạo đối tượng họcsinhDTO và gán giá trị thuộc tính tưng đối tượng
            PhieuXuatETT pxe = new PhieuXuatETT();
            pxe.MaKH1 = cbtenkh.SelectedValue.ToString();
            pxe.MaHang1 = cbtenhang.SelectedValue.ToString();
            pxe.NgayBan1 = DateTime.Parse(dtngayban.Text);
            pxe.MaPX1 = tbmapx.Text;
            pxe.SoLuongBan1 = int.Parse(tbsoluong.Text);
            pxe.GiaBan1 = int.Parse(tbgiaban.Text);
           
            //Xử lý thêm học sinh
            if (BUSPhieuXuat.suaphieuxuat(pxe) == true)
            {
                loadphieuxuat();
                tinhtongsp();
                MessageBox.Show("Sửa thành công!", "Thông Báo");
                return;
            }

            MessageBox.Show("Sửa thất bại!", "Thông Báo");
        }

        private void btxoa_Click(object sender, EventArgs e)
        {
            this.textBox1.Enabled = false;
            this.tbmapx.Enabled = false;
            PhieuXuatETT pxe = new PhieuXuatETT();
            pxe.MaPX1 = tbmapx.Text;
            if ( BUSPhieuXuat.xoaphieuxuat(pxe) == true)
            {
                loadphieuxuat();
                tinhtongsp();
                MessageBox.Show("Xóa Thành Công!", "Thông Báo");
                return;
            }
            MessageBox.Show("Xóa Thất Bại!", "Thông Báo");
        }

        private void bthuy_Click(object sender, EventArgs e)
        {
            loadphieuxuat();
            this.textBox1.Enabled = false;
            this.tbtk.ResetText();
            this.tbmapx.ResetText();
            this.cbtenkh.ResetText();
            this.cbtenhang.ResetText();
            this.dtngayban.ResetText();
            this.tbsoluong.ResetText();
            this.tbgiaban.ResetText();
            this.tbmapx.Enabled = false;
        }

        private void btthoat_Click(object sender, EventArgs e)
        {
            Mainc pt = new Mainc();
            this.Close();
            pt.Show();
        }

        private void bttimkiem_Click(object sender, EventArgs e)
        {
            this.tbmapx.Enabled = false;
            if (tbtk.Text == "")
            {
                MessageBox.Show("Ban Chưa nhập từ khóa", "Thông Báo");
                return;
            }
            string timkiem = tbtk.Text;
            DataTable dt = BUSPhieuXuat.timkiempx(timkiem);
            if (dt.Rows.Count == 0)
            {
                MessageBox.Show("Không Tìm Thấy Kết Quả", "Thông Báo");
                return;
            }
            MessageBox.Show(string.Format("Đã Tìm Thấy {0} Kết Quả", dt.Rows.Count), "Thông Báo");
            dgvphieuxuat.DataSource = dt;
        }

        public void tinhtongsp()
        {   //Tinh tổng có bn cột
            //int sp = dgvphieuxuat.Rows.Count;
            //sp = sp - 1;
            //textBox1.Text = sp.ToString();

            //Tính tổng all tiền
            int tien = dgvphieuxuat.Rows.Count;
            int thanhtien = 0;
            for (int i = 0; i < tien - 1; i++)
            {
                thanhtien += int.Parse(dgvphieuxuat.Rows[i].Cells["Thanhtien"].Value.ToString());
            }
            textBox1.Text = thanhtien.ToString();
        }

        private void thongke_Click(object sender, EventArgs e)
        {
            BaoCaoXuat bcx = new BaoCaoXuat();
            bcx.Show();
        }

    }
}