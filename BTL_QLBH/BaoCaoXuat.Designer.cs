﻿namespace BTL_QLBH
{
    partial class BaoCaoXuat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.ThongKeNhapBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.BTL_QLBHDataSet1 = new BTL_QLBH.BTL_QLBHDataSet1();
            this.ReportXuatBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.BTL_QLBHDataSet = new BTL_QLBH.BTL_QLBHDataSet();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.dtfromyear = new System.Windows.Forms.DateTimePicker();
            this.dttoyear = new System.Windows.Forms.DateTimePicker();
            this.thongke = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.PhieuXuatBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.PhieuXuatTableAdapter = new BTL_QLBH.BTL_QLBHDataSetTableAdapters.PhieuXuatTableAdapter();
            this.ReportXuatTableAdapter = new BTL_QLBH.BTL_QLBHDataSetTableAdapters.ReportXuatTableAdapter();
            this.ThongKeNhapTableAdapter = new BTL_QLBH.BTL_QLBHDataSet1TableAdapters.ThongKeNhapTableAdapter();
            this.ThongKeXuatBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ThongKeXuatTableAdapter = new BTL_QLBH.BTL_QLBHDataSet1TableAdapters.ThongKeXuatTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.ThongKeNhapBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BTL_QLBHDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportXuatBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BTL_QLBHDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PhieuXuatBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ThongKeXuatBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // ThongKeNhapBindingSource
            // 
            this.ThongKeNhapBindingSource.DataMember = "ThongKeNhap";
            this.ThongKeNhapBindingSource.DataSource = this.BTL_QLBHDataSet1;
            // 
            // BTL_QLBHDataSet1
            // 
            this.BTL_QLBHDataSet1.DataSetName = "BTL_QLBHDataSet1";
            this.BTL_QLBHDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // ReportXuatBindingSource
            // 
            this.ReportXuatBindingSource.DataMember = "ReportXuat";
            this.ReportXuatBindingSource.DataSource = this.BTL_QLBHDataSet;
            // 
            // BTL_QLBHDataSet
            // 
            this.BTL_QLBHDataSet.DataSetName = "BTL_QLBHDataSet";
            this.BTL_QLBHDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // reportViewer1
            // 
            reportDataSource1.Name = "DataSet1";
            reportDataSource1.Value = this.ThongKeXuatBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "BTL_QLBH.Report6.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(28, 73);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(760, 287);
            this.reportViewer1.TabIndex = 0;
            // 
            // dtfromyear
            // 
            this.dtfromyear.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtfromyear.Location = new System.Drawing.Point(75, 47);
            this.dtfromyear.Name = "dtfromyear";
            this.dtfromyear.Size = new System.Drawing.Size(160, 20);
            this.dtfromyear.TabIndex = 1;
            // 
            // dttoyear
            // 
            this.dttoyear.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dttoyear.Location = new System.Drawing.Point(304, 47);
            this.dttoyear.Name = "dttoyear";
            this.dttoyear.Size = new System.Drawing.Size(160, 20);
            this.dttoyear.TabIndex = 2;
            // 
            // thongke
            // 
            this.thongke.Location = new System.Drawing.Point(470, 44);
            this.thongke.Name = "thongke";
            this.thongke.Size = new System.Drawing.Size(75, 23);
            this.thongke.TabIndex = 3;
            this.thongke.Text = "Thống Kê";
            this.thongke.UseVisualStyleBackColor = true;
            this.thongke.Click += new System.EventHandler(this.thongke_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Location = new System.Drawing.Point(254, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(302, 29);
            this.label1.TabIndex = 4;
            this.label1.Text = "THỐNG KÊ XUẤT HÀNG";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Từ Ngày:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(241, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Đến Ngày:";
            // 
            // PhieuXuatBindingSource
            // 
            this.PhieuXuatBindingSource.DataMember = "PhieuXuat";
            this.PhieuXuatBindingSource.DataSource = this.BTL_QLBHDataSet;
            // 
            // PhieuXuatTableAdapter
            // 
            this.PhieuXuatTableAdapter.ClearBeforeFill = true;
            // 
            // ReportXuatTableAdapter
            // 
            this.ReportXuatTableAdapter.ClearBeforeFill = true;
            // 
            // ThongKeNhapTableAdapter
            // 
            this.ThongKeNhapTableAdapter.ClearBeforeFill = true;
            // 
            // ThongKeXuatBindingSource
            // 
            this.ThongKeXuatBindingSource.DataMember = "ThongKeXuat";
            this.ThongKeXuatBindingSource.DataSource = this.BTL_QLBHDataSet1;
            // 
            // ThongKeXuatTableAdapter
            // 
            this.ThongKeXuatTableAdapter.ClearBeforeFill = true;
            // 
            // BaoCaoXuat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(813, 384);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.thongke);
            this.Controls.Add(this.dttoyear);
            this.Controls.Add(this.dtfromyear);
            this.Controls.Add(this.reportViewer1);
            this.Name = "BaoCaoXuat";
            this.Text = "BaoCaoXuat";
            this.Load += new System.EventHandler(this.BaoCaoXuat_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ThongKeNhapBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BTL_QLBHDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportXuatBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BTL_QLBHDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PhieuXuatBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ThongKeXuatBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource PhieuXuatBindingSource;
        private BTL_QLBHDataSet BTL_QLBHDataSet;
        private BTL_QLBHDataSetTableAdapters.PhieuXuatTableAdapter PhieuXuatTableAdapter;
        private System.Windows.Forms.BindingSource ReportXuatBindingSource;
        private BTL_QLBHDataSetTableAdapters.ReportXuatTableAdapter ReportXuatTableAdapter;
        private System.Windows.Forms.DateTimePicker dtfromyear;
        private System.Windows.Forms.DateTimePicker dttoyear;
        private System.Windows.Forms.Button thongke;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.BindingSource ThongKeNhapBindingSource;
        private BTL_QLBHDataSet1 BTL_QLBHDataSet1;
        private BTL_QLBHDataSet1TableAdapters.ThongKeNhapTableAdapter ThongKeNhapTableAdapter;
        private System.Windows.Forms.BindingSource ThongKeXuatBindingSource;
        private BTL_QLBHDataSet1TableAdapters.ThongKeXuatTableAdapter ThongKeXuatTableAdapter;
    }
}