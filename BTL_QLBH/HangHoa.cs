﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using ETT;
using BUS;
namespace BTL_QLBH
{
    public partial class FormHangHoa : Form
    {
        
        public FormHangHoa()
        {
            InitializeComponent();
        }

        private void FormHangHoa_Load(object sender, EventArgs e)
        {
            loadhanghoa();
        }
        private void loadhanghoa()
        {
            this.tbmahang.Enabled = false;
            DataTable dt = BUSHangHoa.laydulieuhanghoa();
            dgvhanghoa.DataSource = dt;
        }
        private void Matutang()
        {

            DataTable dt = BUSHangHoa.laydulieuhanghoa();
            dgvhanghoa.DataSource = dt;
            string s = "";
            if (dt.Rows.Count <= 0)
                s = "MAH001";
            else
            {
                int k;
                s = "MAH";
                k = Convert.ToInt32(dt.Rows[dt.Rows.Count - 1][0].ToString().Substring(3, 3));
                k = k + 1;
                if (k < 10) s = s + "00";
                else if (k < 100)
                    s = s + "0";
                //else if (k < 1000)
                //    s = s + "00";
                //else if (k < 10000)
                //    s = s + "0";
                s = s + k.ToString();
            }
            tbmahang.Text = s;
        }

        private void btthem_Click(object sender, EventArgs e)
        {
            this.tbmahang.Enabled = false;
            if (tbtenhang.Text == "")
            {
                MessageBox.Show("Bạn Hãy Nhập Đầy Đủ Thông Tin", "Thông Báo");
                return;
            }
                Matutang();
                HangHoaETT hhe =new HangHoaETT();
                hhe.MaHang1 = tbmahang.Text;
                hhe.TenHang1=tbtenhang.Text;
                hhe.LoaiHang1=tbloaihang.Text;
                hhe.DonViTinh1=tbdvt.Text;
                if(BUSHangHoa.themhanghoa(hhe)==true){
                    loadhanghoa();
                    MessageBox.Show("Thêm Thành Công","Thông Báo");
                    return;
                }
                MessageBox.Show("Thêm Thất Bại","Thông Báo");
        }

        private void thsua_Click(object sender, EventArgs e)
        {
            this.tbmahang.Enabled = false;
            if (tbtenhang.Text == "" || tbloaihang.Text == "" || tbdvt.Text == "")
            {
                MessageBox.Show("Bạn Hãy Nhập Đầy Đủ Thông Tin", "Thông Báo");
                return;
            }
            //Matutang();
            HangHoaETT hhe = new HangHoaETT();
            hhe.MaHang1 = tbmahang.Text;
            hhe.TenHang1 = tbtenhang.Text;
            hhe.LoaiHang1 = tbloaihang.Text;
            hhe.DonViTinh1 = tbdvt.Text;
            if (BUSHangHoa.suahanghoa(hhe) == true)
            {
                
                loadhanghoa();
                MessageBox.Show("Cập Nhật Thành Công", "Thông Báo");
                return;
            }
            MessageBox.Show("Cập Nhật Thất Bại", "Thông Báo");
        }

        private void btxoa_Click(object sender, EventArgs e)
        {
            this.tbmahang.Enabled = false;
            HangHoaETT hhe = new HangHoaETT();
            hhe.MaHang1 = tbmahang.Text;
            if(BUSHangHoa.xoahanghoa(hhe)==true){
                loadhanghoa();
                MessageBox.Show("Xóa Thành Công!","Thông Báo");
                return;
            }
            MessageBox.Show("Xóa Thất Bại!","Thông Báo");
        }

        private void huy_Click(object sender, EventArgs e)
        {
            this.tbmahang.ResetText();
            this.tbtenhang.ResetText();
            this.tbloaihang.ResetText();
            this.tbdvt.ResetText();
            this.tbmahang.Enabled = false;
            loadhanghoa();
            this.tbtimkiem.ResetText();
        }

        private void bttimkiem_Click(object sender, EventArgs e)
        {
            //rbmh.Click = true;
            if (rbth.Checked == true)
            {
                rbmh.Checked = false;
                this.tbmahang.Enabled = false;
                if (tbtimkiem.Text == "")
                {
                    MessageBox.Show("Ban Chưa nhập từ khóa", "Thông Báo");
                    return;
                }
                string timkiem = tbtimkiem.Text;
                DataTable dt = BUSHangHoa.timkiemhh(timkiem);
                if (dt.Rows.Count == 0)
                {
                    MessageBox.Show("Không Tìm Thấy Kết Quả", "Thông Báo");
                    return;
                }
                MessageBox.Show(string.Format("Đã Tìm Thấy {0} Kết Quả", dt.Rows.Count), "Thông Báo");
                dgvhanghoa.DataSource = dt;
            }
            else
            {
                this.tbmahang.Enabled = false;
                if (tbtimkiem.Text == "")
                {
                    MessageBox.Show("Ban Chưa nhập từ khóa", "Thông Báo");
                    return;
                }
                string timkiem = tbtimkiem.Text;
                DataTable dt = BUSHangHoa.timheomahang(timkiem);
                if (dt.Rows.Count == 0)
                {
                    MessageBox.Show("Không Tìm Thấy Kết Quả", "Thông Báo");
                    return;
                }
                MessageBox.Show(string.Format("Đã Tìm Thấy {0} Kết Quả", dt.Rows.Count), "Thông Báo");
                dgvhanghoa.DataSource = dt;
            }
        }

        private void btthoat_Click(object sender, EventArgs e)
        {
            Mainc pt = new Mainc();
            this.Close();
            pt.Show();
        }

        private void dgvhanghoa_Click(object sender, EventArgs e)
        {
            int r = dgvhanghoa.CurrentCell.RowIndex;
            this.tbmahang.Text = dgvhanghoa.Rows[r].Cells[0].Value.ToString();
            this.tbtenhang.Text = dgvhanghoa.Rows[r].Cells[1].Value.ToString();
            this.tbloaihang.Text = dgvhanghoa.Rows[r].Cells[2].Value.ToString();
            this.tbdvt.Text = dgvhanghoa.Rows[r].Cells[3].Value.ToString();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

       
            
        }

        
        
    }

