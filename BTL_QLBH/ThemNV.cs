﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BUS;
using ETT;
using System.Data.SqlClient;

namespace BTL_QLBH
{
    public partial class ThemNV : Form
    {
        public ThemNV()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Mainc pt = new Mainc();
            this.Close();
            pt.Show();
        }

        private void btthem_Click(object sender, EventArgs e)
        {
            if (tbtk.Text == "")
            {
                MessageBox.Show("Chưa Nhập Tên Tài Khoản", "Thông Báo");
            }
            else if (tbmk.Text == "")
            {
                MessageBox.Show("Chưa Nhập Mật Khẩu", "Thông Báo");
            }
            else if (tbht.Text == "")
            {
                MessageBox.Show("Chưa Nhập Họ Tên", "Thông Báo");
            }
            else if(cbquyen.Text=="")
            {
                MessageBox.Show("Chưa Nhập Quyền", "Thông Báo");
            }
            DangNhapETT dne = new DangNhapETT();
            dne.TenDN1 = tbtk.Text;
            dne.MatKhau1 = tbmk.Text;
            dne.HoTen1 = tbht.Text;
            dne.Quyen1 = cbquyen.Text;
            dne.TrangThai1 = cbtt.Text;
           
          
            if (BUSDangNhap.themnv(dne) == true)
            {
                loadthemnv();
                MessageBox.Show("Thêm Thành Công","Thông Báo");
                    return;
                }
                MessageBox.Show("Thêm Thất Bại","Thông Báo");
            
        }
        private void loadthemnv()
        {
            DataTable dt = BUSDangNhap.loaddn();
            dgvthemtk.DataSource = dt;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (tbtk.Text == "" || tbmk.Text == "" || tbht.Text == ""||cbquyen.Text==""||cbtt.Text=="")
            {
                MessageBox.Show("Bạn Hãy Nhập Đầy Đủ Thông Tin", "Thông Báo");
                return;
            }
            DangNhapETT dne = new DangNhapETT();
            dne.TrangThai1 = cbtt.Text;
            dne.TenDN1 = tbtk.Text;
            dne.MatKhau1 = tbmk.Text;
            dne.HoTen1 = tbht.Text;
            dne.Quyen1 = cbquyen.Text;
            if (cbtt.Text == "Khóa")
            {
                dne.Loi = 5;
            }
            else
            {
                dne.Loi = 0;
            }
            if (BUSDangNhap.suanv(dne) == true)
            {
               loadthemnv();
                MessageBox.Show("Cập Nhật Thành Công", "Thông Báo");
                return;
            }
            MessageBox.Show("Cập Nhật Thất Bại", "Thông Báo");
        }
        

        private void ThemNV_Load(object sender, EventArgs e)
        {
            loadthemnv();
        }

        private void dgvthemtk_Click(object sender, EventArgs e)
        {
            int r = dgvthemtk.CurrentCell.RowIndex;
            this.tbtk.Text = dgvthemtk.Rows[r].Cells[0].Value.ToString();
            this.tbmk.Text = dgvthemtk.Rows[r].Cells[1].Value.ToString();
            this.cbquyen.Text = dgvthemtk.Rows[r].Cells[2].Value.ToString();
            this.cbtt.Text = dgvthemtk.Rows[r].Cells[3].Value.ToString();
            this.tbht.Text = dgvthemtk.Rows[r].Cells[4].Value.ToString();
        }

        private void btxoa_Click(object sender, EventArgs e)
        {
            DangNhapETT dne = new DangNhapETT();
            dne.TenDN1 = tbtk.Text;
            if(BUSDangNhap.xoanv(dne) == true)
            {
                loadthemnv();
                MessageBox.Show("Xóa Thành Công", "Thông Báo");
                return;
            }
            MessageBox.Show("Xóa Thất Bại", "Thông Báo");
        
            
        }

        private void btreload_Click(object sender, EventArgs e)
        {
            loadthemnv();
            this.tbtk.ResetText();
            this.tbmk.ResetText();
            this.tbht.ResetText();
            this.textBox2.ResetText();
            this.cbquyen.ResetText();
            this.cbtt.ResetText();
        }

        private void bttimkiem_Click(object sender, EventArgs e)
        {
            if (textBox2.Text == "")
            {
                MessageBox.Show("Ban Chưa nhập từ khóa", "Thông Báo");
                return;
            }
            string timkiem = textBox2.Text;
            DataTable dt = BUSDangNhap.timkiemnv(timkiem);
            if (dt.Rows.Count == 0)
            {
                loadthemnv();
                MessageBox.Show("Không Tìm Thấy Kết Quả", "Thông Báo");
                return;
            }
            MessageBox.Show(string.Format("Đã Tìm Thấy {0} Kết Quả", dt.Rows.Count), "Thông Báo");
            dgvthemtk.DataSource = dt;
        }

        
      
    }
}
