﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace BTL_QLBH
{
    public partial class BaoCaoXuat : Form
    {
        public BaoCaoXuat()
        {
            InitializeComponent();
        }
        //DataView dv ;
        private void BaoCaoXuat_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'BTL_QLBHDataSet1.ThongKeXuat' table. You can move, or remove it, as needed.
            this.ThongKeXuatTableAdapter.Fill(this.BTL_QLBHDataSet1.ThongKeXuat);
            // TODO: This line of code loads data into the 'BTL_QLBHDataSet1.ThongKeNhap' table. You can move, or remove it, as needed.
            //this.ThongKeNhapTableAdapter.Fill(this.BTL_QLBHDataSet1.ThongKeXuat);
            // TODO: This line of code loads data into the 'BTL_QLBHDataSet.ReportXuat' table. You can move, or remove it, as needed.
            //this.ReportXuatTableAdapter.Fill(this.BTL_QLBHDataSet.ReportXuat);
            // TODO: This line of code loads data into the 'BTL_QLBHDataSet.PhieuXuat' table. You can move, or remove it, as needed.
            //this.PhieuXuatTableAdapter.Fill(this.BTL_QLBHDataSet.PhieuXuat);
            //dv = new DataView(this.BTL_QLBHDataSet.ReportXuat);
            //this.ReportXuatBindingSource.DataSource = dv;
            Setparameter(dtfromyear.Value, dttoyear.Value);
            this.reportViewer1.RefreshReport();
        }
        private void Setparameter(DateTime fromyear, DateTime toyear)
        {
            ReportParameter[] rp = new ReportParameter[2];
            rp[0] = new ReportParameter("FromYear");
            rp[1] = new ReportParameter("ToYear");
            rp[0].Values.Add(fromyear.ToString());
            rp[1].Values.Add(toyear.ToString());
            reportViewer1.LocalReport.SetParameters(rp);
        }

        private void thongke_Click(object sender, EventArgs e)
        {
            Setparameter(dtfromyear.Value, dttoyear.Value);
            reportViewer1.RefreshReport();

        }

       
    }
}
