﻿namespace BTL_QLBH
{
    partial class Mainc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btncc = new System.Windows.Forms.Button();
            this.btpn = new System.Windows.Forms.Button();
            this.bthh = new System.Windows.Forms.Button();
            this.lbquyen = new System.Windows.Forms.Label();
            this.lbht = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btpx = new System.Windows.Forms.Button();
            this.btkh = new System.Windows.Forms.Button();
            this.btthoat = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btncc
            // 
            this.btncc.Location = new System.Drawing.Point(21, 19);
            this.btncc.Name = "btncc";
            this.btncc.Size = new System.Drawing.Size(75, 43);
            this.btncc.TabIndex = 0;
            this.btncc.Text = "Nhà Cung Cấp";
            this.btncc.UseVisualStyleBackColor = true;
            this.btncc.Click += new System.EventHandler(this.button1_Click);
            // 
            // btpn
            // 
            this.btpn.Location = new System.Drawing.Point(21, 73);
            this.btpn.Name = "btpn";
            this.btpn.Size = new System.Drawing.Size(75, 43);
            this.btpn.TabIndex = 1;
            this.btpn.Text = "Phiếu Nhập";
            this.btpn.UseVisualStyleBackColor = true;
            this.btpn.Click += new System.EventHandler(this.btpn_Click);
            // 
            // bthh
            // 
            this.bthh.Location = new System.Drawing.Point(157, 19);
            this.bthh.Name = "bthh";
            this.bthh.Size = new System.Drawing.Size(75, 43);
            this.bthh.TabIndex = 3;
            this.bthh.Text = "Hàng Hóa";
            this.bthh.UseVisualStyleBackColor = true;
            this.bthh.Click += new System.EventHandler(this.bthh_Click);
            // 
            // lbquyen
            // 
            this.lbquyen.AutoSize = true;
            this.lbquyen.Location = new System.Drawing.Point(23, 195);
            this.lbquyen.Name = "lbquyen";
            this.lbquyen.Size = new System.Drawing.Size(43, 13);
            this.lbquyen.TabIndex = 4;
            this.lbquyen.Text = "Status1";
            // 
            // lbht
            // 
            this.lbht.AutoSize = true;
            this.lbht.Location = new System.Drawing.Point(23, 217);
            this.lbht.Name = "lbht";
            this.lbht.Size = new System.Drawing.Size(43, 13);
            this.lbht.TabIndex = 4;
            this.lbht.Text = "Status2";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.btpx);
            this.groupBox1.Controls.Add(this.btkh);
            this.groupBox1.Controls.Add(this.bthh);
            this.groupBox1.Controls.Add(this.btncc);
            this.groupBox1.Controls.Add(this.btpn);
            this.groupBox1.Location = new System.Drawing.Point(26, 52);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(436, 134);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông Tin";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(157, 73);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 43);
            this.button1.TabIndex = 6;
            this.button1.Text = "Tài Khoản";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // btpx
            // 
            this.btpx.Location = new System.Drawing.Point(313, 73);
            this.btpx.Name = "btpx";
            this.btpx.Size = new System.Drawing.Size(75, 43);
            this.btpx.TabIndex = 5;
            this.btpx.Text = "Phiếu Xuất";
            this.btpx.UseVisualStyleBackColor = true;
            this.btpx.Click += new System.EventHandler(this.btpx_Click);
            // 
            // btkh
            // 
            this.btkh.Location = new System.Drawing.Point(313, 19);
            this.btkh.Name = "btkh";
            this.btkh.Size = new System.Drawing.Size(75, 43);
            this.btkh.TabIndex = 4;
            this.btkh.Text = "Khách Hàng";
            this.btkh.UseVisualStyleBackColor = true;
            this.btkh.Click += new System.EventHandler(this.btkh_Click);
            // 
            // btthoat
            // 
            this.btthoat.Location = new System.Drawing.Point(387, 192);
            this.btthoat.Name = "btthoat";
            this.btthoat.Size = new System.Drawing.Size(75, 23);
            this.btthoat.TabIndex = 6;
            this.btthoat.Text = "Thoát";
            this.btthoat.UseVisualStyleBackColor = true;
            this.btthoat.Click += new System.EventHandler(this.btthoat_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Location = new System.Drawing.Point(21, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(433, 24);
            this.label1.TabIndex = 7;
            this.label1.Text = "QUẢN LÝ BÁN HÀNG CÔNG TY MAI HOÀNG";
            // 
            // Mainc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(486, 248);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btthoat);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lbht);
            this.Controls.Add(this.lbquyen);
            this.Name = "Mainc";
            this.Text = "Quản Lý Bán Hàng Công Ty Mai Hoàng V1.0";
            this.Load += new System.EventHandler(this.Main_Load);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btncc;
        private System.Windows.Forms.Button btpn;
        private System.Windows.Forms.Button bthh;
        private System.Windows.Forms.Label lbquyen;
        private System.Windows.Forms.Label lbht;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btpx;
        private System.Windows.Forms.Button btkh;
        private System.Windows.Forms.Button btthoat;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
    }
}