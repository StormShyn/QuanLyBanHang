﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT
{
    public class PhieuNhapETT
    {
        private string MaPN;

        public string MaPN1
        {
            get { return MaPN; }
            set { MaPN = value; }
        }
        private string MaNCC;

        public string MaNCC1
        {
            get { return MaNCC; }
            set { MaNCC = value; }
        }
        private string MaHang;

        public string MaHang1
        {
            get { return MaHang; }
            set { MaHang = value; }
        }
        private DateTime NgayNhap;

        public DateTime NgayNhap1
        {
            get { return NgayNhap; }
            set { NgayNhap = value; }
        }
        private int SoLuongNhap;

        public int SoLuongNhap1
        {
            get { return SoLuongNhap; }
            set { SoLuongNhap = value; }
        }
        private int GiaNhap;

        public int GiaNhap1
        {
            get { return GiaNhap; }
            set { GiaNhap = value; }
        }
    }
}
