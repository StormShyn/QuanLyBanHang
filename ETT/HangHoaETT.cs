﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace ETT
{
    public class HangHoaETT
    {
        private string MaHang;

        public string MaHang1
        {
            get { return MaHang; }
            set { MaHang = value; }
        }
        private string TenHang;

        public string TenHang1
        {
            get { return TenHang; }
            set { TenHang = value; }
        }
        private string LoaiHang;

        public string LoaiHang1
        {
            get { return LoaiHang; }
            set { LoaiHang = value; }
        }
        private string DonViTinh;

        public string DonViTinh1
        {
            get { return DonViTinh; }
            set { DonViTinh = value; }
        }
        private int Hangcon;
        public int HangCon1
        {
            get { return Hangcon; }
            set { Hangcon = value; }
        }
    }
}
