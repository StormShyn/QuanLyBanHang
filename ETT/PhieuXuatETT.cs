﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT
{
   public  class PhieuXuatETT
    {
        private string MaPX;
        private string MaKH;
        private string MaHang;
        private DateTime NgayBan;
        private int SoLuongBan;
        private int GiaBan;
        public string MaKH1
        {
            get { return MaKH; }
            set { MaKH = value; }
        }
       
        public int GiaBan1
        {
            get { return GiaBan; }
            set { GiaBan = value; }
        }

        public int SoLuongBan1
        {
            get { return SoLuongBan; }
            set { SoLuongBan = value; }
        }

        public DateTime NgayBan1
        {
            get { return NgayBan; }
            set { NgayBan = value; }
        }
        public string MaHang1
        {
            get { return MaHang; }
            set { MaHang = value; }
        }

       
        public string MaPX1
        {
            get { return MaPX; }
            set { MaPX = value; }
        }
    }
}
