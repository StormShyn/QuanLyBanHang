﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT
{
   public class DangNhapETT
    {
        private string TenDN;
        private string MatKhau;
        private string Quyen;
        private string ChucDanh;
        private string HoTen;
        private string TrangThai;
        private int loi;

        public int Loi
        {
            get { return loi; }
            set { loi = value; }
        }

        public string TrangThai1
        {
            get { return TrangThai; }
            set { TrangThai = value; }
        }
        public string HoTen1
        {
            get { return HoTen; }
            set { HoTen = value; }
        }

        public string ChucDanh1
        {
            get { return ChucDanh; }
            set { ChucDanh = value; }
        }

        public string Quyen1
        {
            get { return Quyen; }
            set { Quyen = value; }
        }

        public string MatKhau1
        {
            get { return MatKhau; }
            set { MatKhau = value; }
        }

        public string TenDN1
        {
            get { return TenDN; }
            set { TenDN = value; }
        }
    }
}
