﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT
{
   public  class KhachHangETT
    {
        private string MaKH;

        public string MaKH1
        {
            get { return MaKH; }
            set { MaKH = value; }
        }
        private string TenKH;

        public string TenKH1
        {
            get { return TenKH; }
            set { TenKH = value; }
        }
        private string DiaChiKH;
        private string DienThoaiKH;

        public string DienThoaiKH1
        {
            get { return DienThoaiKH; }
            set { DienThoaiKH = value; }
        }

        public string DiaChiKH1
        {
            get { return DiaChiKH; }
            set { DiaChiKH = value; }
        }
    }
}
