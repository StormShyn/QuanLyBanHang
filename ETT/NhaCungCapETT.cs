﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT
{
    public class NhaCungCapETT
    {
        private string MaNCC;

        public string MaNCC1
        {
            get { return MaNCC; }
            set { MaNCC = value; }
        }
        private string TenNCC;

        public string TenNCC1
        {
            get { return TenNCC; }
            set { TenNCC = value; }
        }
        private string DiaChiNCC;

        public string DiaChiNCC1
        {
            get { return DiaChiNCC; }
            set { DiaChiNCC = value; }
        }
        private string DienThoaiNCC;

        public string DienThoaiNCC1
        {
            get { return DienThoaiNCC; }
            set { DienThoaiNCC = value; }
        }
    }
}
