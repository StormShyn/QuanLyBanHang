﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DAL;
using ETT;
namespace BUS
{
   public  class BUSNhaCungCap
    {
        public static DataTable laydulieunhacungcap()
        {
            //Gọi hàm xử lý bảng
            return DALNhaCungCap.laydulieunhacungcap();
        }
        public static bool themnhacungcap(NhaCungCapETT ncce)
        {
            return DALNhaCungCap.themnhacungcap(ncce);
        }
        public static bool suanhacungcap(NhaCungCapETT ncce)
        {
            return DALNhaCungCap.suanhacungcap(ncce);
        }
        public static bool xoanhacc(NhaCungCapETT ncce)
        {
            return DALNhaCungCap.xoanhacc(ncce);
        }
        public static DataTable timkiemncc(string timkiem)
        {
            return DALNhaCungCap.timkiemncc(timkiem);
        }
        public static DataTable timkiemmancc(string timkiem)
        {
            return DALNhaCungCap.timkiemmancc(timkiem);
        }
    }
}
