﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DAL;
using ETT;
namespace BUS
{
   public class BUSPhieuXuat
    {
        public static DataTable laydulieuphieuxuat()
        {
            //Gọi hàm xử lý bảng
            return DALPhieuXuat.loadphieuxuat();
        }
        public static bool themphieuxuat(PhieuXuatETT pxe)
        {
            return DALPhieuXuat.themphieuxuat(pxe);
        }
        public static bool suaphieuxuat(PhieuXuatETT pxe)
        {
            return DALPhieuXuat.suaphieuxuat(pxe);
        }
        public static bool xoaphieuxuat(PhieuXuatETT pxe)
        {
            return DALPhieuXuat.xoaphieuxuat(pxe);
        }
        public static DataTable timkiempx(string timkiem)
        {
            return DALPhieuXuat.timkiempx(timkiem);
        }
       
    }
}
