﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DAL;
using ETT;
namespace BUS
{
    public class BUSHangHoa
    {
        public static DataTable laydulieuhanghoa()
        {
            //Gọi hàm xử lý bảng
            return DALHangHoa.laydulieuhanghoa();
        }
        public static bool themhanghoa(HangHoaETT hhe)
        {
            return DALHangHoa.themhanghoa(hhe);
        }
        public static bool suahanghoa(HangHoaETT hhe)
        {
            return DALHangHoa.suahanghoa(hhe);
        }
        public static bool xoahanghoa(HangHoaETT hhe)
        {
            return DALHangHoa.xoahanghoa(hhe);
        }
        public static DataTable timkiemhh(string timkiem)
        {
            return DALHangHoa.timkiemhh(timkiem);
        }
        public static DataTable timheomahang(string timkiem)
        {
            return DALHangHoa.timtheomahang(timkiem);
        }
    }
}
