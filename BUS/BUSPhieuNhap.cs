﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DAL;
using ETT;
namespace BUS
{
    public class BUSPhieuNhap
    {
        public static DataTable laydulieuphieunhap()
        {
            //Gọi hàm xử lý bảng
            return DALPhieuNhap.laydulieuphieunhap();
        }
        public static bool themphieunhap(PhieuNhapETT pne)
        {
            return DALPhieuNhap.themphieuhang(pne);
        }
        public static bool suaphieuhang(PhieuNhapETT pne)
        {
            return DALPhieuNhap.suaphieuhang(pne);
        }
        public static bool xoaphieuhang(PhieuNhapETT pne)
        {
            return DALPhieuNhap.xoaphieuhang(pne);
        }
        public static DataTable timkiempn(string timkiem)
        {
            return DALPhieuNhap.timkiempn(timkiem);
        }
    }
}
