﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DAL;
using ETT;
namespace BUS
{
   public class BUSDangNhap
    {
       public static DataTable loaddn()
       {
           return DALDangNhap.loaddn();
       }
      
       //public static DataTable kttendn(DangNhapETT dne)
       //{
       //    return DALDangNhap.kttendn(dne);
       //}
       //public static DataTable ktmatkhau(DangNhapETT dne)
       //{
       //    return DALDangNhap.ktmatkhau(dne);
       //}
       public static bool themnv(DangNhapETT dne)
       {
           return DALDangNhap.themnv(dne);
       }
       public static bool suanv(DangNhapETT dne)
       {
           return DALDangNhap.suanv(dne);
       }

       public static bool xoanv(DangNhapETT dne)
       {
           return DALDangNhap.xoanv(dne);
       }
       public static DataTable timkiemnv(string timkiem)
       {
           return DALDangNhap.timkiemnv(timkiem);
       }

    }
}
