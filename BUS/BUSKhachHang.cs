﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DAL;
using ETT;
namespace BUS
{
    public class BUSKhachHang
    {
        public static DataTable loadkhachhang()
        {
            return DALKhachHang.loadkhachhang();
        }
        public static bool themkhachhang(KhachHangETT khe){
            return DALKhachHang.themkhachhang(khe);
        }
        public static bool suakhachhang(KhachHangETT khe)
        {
            return DALKhachHang.suakhachhang(khe);
        }
        public static bool xoakhachhang(KhachHangETT khe)
        {
            return DALKhachHang.xoakhachhang(khe);
        }
        public static DataTable timkiemkhachhang(string timkiem)
        {
            return DALKhachHang.timkiemkh(timkiem);
        }
        public static DataTable timkiemmakh(string timkiem)
        {
            return DALKhachHang.timkiemmakh(timkiem);
        }
    }
}
