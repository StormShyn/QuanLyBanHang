﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using ETT;

namespace DAL
{
    public class DALPhieuNhap
    {
        static SqlConnection con;
        //Lấy dữ liệu từ bảng Phiếu Nhập

        public static DataTable laydulieuphieunhap()
        {
            //string truyvan = "Select MaPN,NhaCungCap.MaNCC,NhaCungCap.TenNCC,NhaCungCap.DiaChiNCC,NhaCungCap.DienThoaiNCC,HangHoa.MaHang,HangHoa.TenHang,HangHoa.LoaiHang,HangHoa.DonViTinh,NgayNhap,SoLuongNhap,GiaNhap From PhieuNhap inner join HangHoa on PhieuNhap.MaHang=HangHoa.MaHang inner join NhaCungCap on PhieuNhap.MaNCC=NhaCungCap.MaNCC";
            string truyvan = "Select MaPN,MaNCC,MaHang,NgayNhap,SoLuongNhap,GiaNhap,SoLuongNhap*GiaNhap as ThanhTien from PhieuNhap";
            con = DBConnection.KeNoi();
            DataTable dt = DBConnection.LayDuLieuBang(truyvan, con);
            DBConnection.DongKetNoi(con);
            return dt;
        }
        public static bool themphieuhang(PhieuNhapETT pne)
        {
            try
            {
                string truyvan = string.Format("Insert into PhieuNhap values ('"+pne.MaPN1+"','" + pne.MaNCC1 + "','" + pne.MaHang1 + "','" + pne.NgayNhap1 + "','" + pne.SoLuongNhap1 + "','" + pne.GiaNhap1 + "')");
                con = DBConnection.KeNoi();
                DBConnection.thucthitruyvan(truyvan, con);
                DBConnection.DongKetNoi(con);
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool suaphieuhang(PhieuNhapETT pne)
        {
            try
            {
                string truyvan = string.Format("Update PhieuNhap set MaNCC='{0}',MaHang='{1}',NgayNhap='{2}',SoLuongNhap='{3}',GiaNhap='{4}' where MaPN='{5}'", pne.MaNCC1, pne.MaHang1, pne.NgayNhap1, pne.SoLuongNhap1,pne.GiaNhap1,pne.MaPN1);
                con = DBConnection.KeNoi();
                DBConnection.thucthitruyvan(truyvan, con);
                DBConnection.DongKetNoi(con);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool xoaphieuhang(PhieuNhapETT pne)
        {
            try
            {
                string truyvan = string.Format("Delete From PhieuNhap where MaPN='" + pne.MaPN1 + "'");
                con = DBConnection.KeNoi();
                DBConnection.thucthitruyvan(truyvan, con);
                DBConnection.DongKetNoi(con);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
                    }
        public static DataTable timkiempn(string timkiem)
        {
            string truyvan = string.Format("Select * from PhieuNhap where MaPN like '%" + timkiem + "%'");
            con = DBConnection.KeNoi();
            DataTable dt = DBConnection.LayDuLieuBang(truyvan, con);
            DBConnection.DongKetNoi(con);
            return dt;
        }

    }
}
