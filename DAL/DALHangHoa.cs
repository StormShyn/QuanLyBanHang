﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using ETT;
namespace DAL
{
    
     public class DALHangHoa
    {
        static SqlConnection con;
         //Lấy dữ liệu từ bảng hàng hóa
        public static int quyen;
        public static DataTable laydulieuhanghoa()
        {
            string truyvan = "Select * From HangHoa";
            con = DBConnection.KeNoi();
            DataTable dt = DBConnection.LayDuLieuBang(truyvan, con);
            DBConnection.DongKetNoi(con);
            return dt;
        }
        public static bool themhanghoa(HangHoaETT hhe)
        {
            try
            {
                string truyvan = "Insert into HangHoa values('"+hhe.MaHang1+"',N'"+hhe.TenHang1+"',N'"+hhe.LoaiHang1+"',N'"+hhe.DonViTinh1+"','"+hhe.HangCon1+"')";
                    con = DBConnection.KeNoi();
                DBConnection.thucthitruyvan(truyvan,con);
                DBConnection.DongKetNoi(con);
                return true;
            }
            catch(Exception){
                return false;
            }
        }
         public static bool suahanghoa(HangHoaETT hhe){
              try
            {
                string truyvan = string.Format("Update HangHoa set TenHang=N'{0}',LoaiHang=N'{1}',DonViTinh=N'{2}' where MaHang='{3}'",hhe.TenHang1,hhe.LoaiHang1,hhe.DonViTinh1,hhe.MaHang1);
                    con = DBConnection.KeNoi();
                DBConnection.thucthitruyvan(truyvan,con);
                DBConnection.DongKetNoi(con);
                return true;
            }
            catch(Exception){
                return false;
            }
         }
         public static bool xoahanghoa(HangHoaETT hhe){
                      try
            {
                string truyvan = string.Format("Delete From HangHoa where MaHang='"+hhe.MaHang1+"'");
                    con = DBConnection.KeNoi();
                DBConnection.thucthitruyvan(truyvan,con);
                DBConnection.DongKetNoi(con);
                return true;
            }
            catch(Exception){
                return false;
            }
         }
         public static DataTable timkiemhh(string timkiem){
             string truyvan = string.Format("Select * From HangHoa where TenHang like N'%"+timkiem+"%'");
             con=DBConnection.KeNoi();
            DataTable dt =  DBConnection.LayDuLieuBang(truyvan,con);
             DBConnection.DongKetNoi(con);
             return dt;
         }
         public static DataTable timtheomahang(string timkiem)
         {
             string truyvan = string.Format("Select * From HangHoa where MaHang like '%" + timkiem + "%'");
             con = DBConnection.KeNoi();
             DataTable dt = DBConnection.LayDuLieuBang(truyvan, con);
             DBConnection.DongKetNoi(con);
             return dt;
         }
    }
}
