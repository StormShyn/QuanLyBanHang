﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using ETT;

namespace DAL
{
    public class DALKhachHang
    {
        static SqlConnection con;
        public static DataTable loadkhachhang()
        {
            string truyvan = "Select * from KhachHang";
            con = DBConnection.KeNoi();
            DataTable dt = DBConnection.LayDuLieuBang(truyvan, con);
            DBConnection.DongKetNoi(con);
            return dt;
        }
        public static bool themkhachhang(KhachHangETT khe)
        {
            try
            {
                string truyvan = string.Format("Insert into KhachHang values ('" + khe.MaKH1 + "',N'" + khe.TenKH1 + "',N'" + khe.DiaChiKH1 + "','" + khe.DienThoaiKH1 + "')");
                con = DBConnection.KeNoi();
                DBConnection.thucthitruyvan(truyvan,con);
                DBConnection.DongKetNoi(con);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
            public static bool suakhachhang(KhachHangETT khe){
                 try
            {
                string truyvan = string.Format("Update KhachHang set TenKH=N'{0}',DiaChiKH=N'{1}',DienThoaiKH='{2}'where MaKH='{3}'",khe.TenKH1,khe.DiaChiKH1,khe.DienThoaiKH1,khe.MaKH1);
                 con = DBConnection.KeNoi();
                DBConnection.thucthitruyvan(truyvan,con);
                DBConnection.DongKetNoi(con);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
            }
        public static bool xoakhachhang(KhachHangETT khe)
        {
            try
            {
                string truyvan = string.Format("Delete From KhachHang where MaKH='"+khe.MaKH1+"'");
                 con = DBConnection.KeNoi();
                DBConnection.thucthitruyvan(truyvan,con);
                DBConnection.DongKetNoi(con);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static DataTable timkiemkh(string timkiem)
        {

            string truyvan = string.Format("Select * From KhachHang where TenKH like N'%" + timkiem + "%'");
            //string truyvan = "Select * From NhaCungCap";
            con = DBConnection.KeNoi();
            DataTable dt = DBConnection.LayDuLieuBang(truyvan, con);
            DBConnection.DongKetNoi(con);
            return dt;

        }
        public static DataTable timkiemmakh(string timkiem)
        {

            string truyvan = string.Format("Select * From KhachHang where MaKH like N'%" + timkiem + "%'");
            //string truyvan = "Select * From NhaCungCap";
            con = DBConnection.KeNoi();
            DataTable dt = DBConnection.LayDuLieuBang(truyvan, con);
            DBConnection.DongKetNoi(con);
            return dt;

        }
        }

    }

