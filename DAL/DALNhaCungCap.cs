﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using ETT;

namespace DAL
{
    public class DALNhaCungCap
    {
        static SqlConnection con;
        //Lấy dữ liệu từ bảng Nhà cung cấp
        
        public static DataTable laydulieunhacungcap()
        {
            string truyvan = "Select * From NhaCungCap";
            con = DBConnection.KeNoi();
            DataTable dt = DBConnection.LayDuLieuBang(truyvan, con);
            DBConnection.DongKetNoi(con);
            return dt;

        }
        public static bool themnhacungcap(NhaCungCapETT ncce)
        {
            try
            {
                string truyvan = string.Format("Insert into NhaCungCap values ('" + ncce.MaNCC1 + "',N'" + ncce.TenNCC1 + "',N'" + ncce.DiaChiNCC1 + "','" + ncce.DienThoaiNCC1 + "')");
                con = DBConnection.KeNoi();
                DBConnection.thucthitruyvan(truyvan, con);
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool suanhacungcap(NhaCungCapETT ncce)
        {
            try
            {
               
                //string truyvan = string.Format("Update NhaCungCap set TenNCC=N'" + ncce.TenNCC1 + "',DiaChiNCC=N'" + ncce.DiaChiNCC1 + "',DienThoaiNCC='" + ncce.DienThoaiNCC1 + "'where MaNCC='" + ncce.MaNCC1 + "'");
               string truyvan = string.Format("Update NhaCungCap set TenNCC=N'{0}',DiaChiNCC=N'{1}',DienThoaiNCC='{2}' where MaNCC='{3}'",ncce.TenNCC1,ncce.DiaChiNCC1,ncce.DienThoaiNCC1,ncce.MaNCC1 );
               con = DBConnection.KeNoi();          
                //update
                
                //truy vấn

                DBConnection.thucthitruyvan(truyvan, con);
                DBConnection.DongKetNoi(con);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool xoanhacc(NhaCungCapETT ncce)
        {
            try{
                string truyvan = string.Format("Delete From NhaCungCap where MaNCC ='"+ncce.MaNCC1+"'");
            con = DBConnection.KeNoi();
            
            DBConnection.thucthitruyvan(truyvan, con);
            DBConnection.DongKetNoi(con);
            return true;
        }
        catch (Exception)
        {
            return false;
        }
        }
        public static DataTable timkiemncc(string timkiem)
        {
            
                string truyvan = string.Format("Select * From NhaCungCap where TenNCC like N'%" + timkiem + "%'");
                //string truyvan = "Select * From NhaCungCap";
                con = DBConnection.KeNoi();
                DataTable dt = DBConnection.LayDuLieuBang(truyvan, con);
                DBConnection.DongKetNoi(con);
                return dt;
         
        }
        public static DataTable timkiemmancc(string timkiem)
        {

            string truyvan = string.Format("Select * From NhaCungCap where MaNCC like N'%" + timkiem + "%'");
            //string truyvan = "Select * From NhaCungCap";
            con = DBConnection.KeNoi();
            DataTable dt = DBConnection.LayDuLieuBang(truyvan, con);
            DBConnection.DongKetNoi(con);
            return dt;

        }
        
        
    }
}
