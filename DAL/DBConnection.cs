﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace DAL
{
    public class DBConnection
    {
        //Kết nối
        public static SqlConnection KeNoi(){
            string chuoikn = @"Data Source=QHONLINE-PC;Initial Catalog=BTL_QLBH;Integrated Security=True";
            //Tạo đối tượng liên kết
        SqlConnection con = new SqlConnection(chuoikn);
        con.Open();
        return con;
        }
        public static void DongKetNoi(SqlConnection con)
        {
            con.Close();
        }

        public static DataTable LayDuLieuBang(string truyvan, SqlConnection con)
        {
            SqlDataAdapter da = new SqlDataAdapter(truyvan, con);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;

        }
        public static void thucthitruyvan(string truyvan, SqlConnection con)
        {
            SqlCommand com = new SqlCommand(truyvan, con);
            com.ExecuteNonQuery();
        }
       
    }
}
