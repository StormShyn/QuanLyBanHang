﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using ETT;

namespace DAL
{
    public class DALPhieuXuat
    {
        static SqlConnection con;
        public static DataTable loadphieuxuat()
        {
            string truyvan = "Select MaPX,MaKH,HangHoa.MaHang,HangHoa.TenHang,NgayBan,SoLuongBan,GiaBan,SoLuongBan*GiaBan as ThanhTien from PhieuXuat inner join HangHoa on PhieuXuat.MaHang=HangHoa.MaHang ";
            con = DBConnection.KeNoi();
            DataTable dt = DBConnection.LayDuLieuBang(truyvan, con);
            DBConnection.DongKetNoi(con);
            return dt;
        }
        public static bool themphieuxuat(PhieuXuatETT pxe)
        {
            try
            {
                string truyvan = string.Format("Insert into PhieuXuat values ('" + pxe.MaPX1 + "','" + pxe.MaKH1 + "','" + pxe.MaHang1 + "','" + pxe.NgayBan1 + "','" + pxe.SoLuongBan1 + "','" + pxe.GiaBan1 + "')");
                con = DBConnection.KeNoi();
                DBConnection.thucthitruyvan(truyvan, con);
                DBConnection.DongKetNoi(con);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool suaphieuxuat(PhieuXuatETT pxe)
        {
            try
            {
                string truyvan = string.Format("Update PhieuXuat set MaKH=N'{0}',MaHang='{1}',NgayBan='{2}',SoLuongBan='{3}',GiaBan='{4}' where MaPX='{5}'",pxe.MaKH1, pxe.MaHang1, pxe.NgayBan1, pxe.SoLuongBan1,pxe.GiaBan1,pxe.MaPX1);
                con = DBConnection.KeNoi();
                DBConnection.thucthitruyvan(truyvan, con);
                DBConnection.DongKetNoi(con);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool xoaphieuxuat(PhieuXuatETT pxe)
        {
            try
            {
                string truyvan = string.Format("Delete From PhieuXuat where MaPX='" + pxe.MaPX1 + "'");
                con = DBConnection.KeNoi();
                DBConnection.thucthitruyvan(truyvan, con);
                DBConnection.DongKetNoi(con);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static DataTable timkiempx(string timkiem)
        {

            string truyvan = string.Format("Select * From PhieuXuat where MaPX like N'%" + timkiem + "%'");
            
            con = DBConnection.KeNoi();
            DataTable dt = DBConnection.LayDuLieuBang(truyvan, con);
            DBConnection.DongKetNoi(con);
            return dt;

        }
       
    }
}
